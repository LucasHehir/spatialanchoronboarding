using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Vanilla.FuncOlympics;
using Vanilla.GeoHasher;

using static UnityEngine.Debug;

public class dumb : MonoBehaviour
{

	public string s;

	public char c;

	public int index;
	
	void Update()
	{
		if (!Input.GetKeyDown(KeyCode.Space)) return;


		Doit();
	}


	private void Doit() => Log(s.Length % 2);


	[ContextMenu("But does it work")]
	public void TestUnrolled()
	{
		var neighbours = GeoHasher.GetNeighboursUnrolled(s);
		
		Log($"Unrolled Neighbour Map\n\n{neighbours[0]}\t{neighbours[1]}\t{neighbours[2]}\n{neighbours[3]}\t{s}\t{neighbours[4]}\n{neighbours[5]}\t{neighbours[6]}\t{neighbours[7]}\n");
		
		var neighbours2 = GeoHasher.GetNeighboursArray(s);
		
		Log($"Unrolled Neighbour Map\n\n{neighbours2[0]}\t{neighbours2[1]}\t{neighbours2[2]}\n{neighbours2[3]}\t{s}\t{neighbours2[4]}\n{neighbours2[5]}\t{neighbours2[6]}\t{neighbours2[7]}\n");

//		Log($"Neighbour Map\n\n{neighbours2["nw"]}\t{neighbours2["n"]}\t{neighbours2["ne"]}\n{neighbours2["w"]}\t{s}\t{neighbours2["e"]}\n{neighbours2["sw"]}\t{neighbours2["s"]}\t{neighbours2["se"]}\n");
		
//		Log($"Neighbour Map\n\n--------\t--------\t{GeoHasher.GetNENeighbour(s, neighbours[1])}\n--------\t{s}\t--------\n--------\t--------\t--------\n");

	}


	[ContextMenu("Func Olympics")]
	public void Olympics() => FuncOlympics.Sprint(a: GeoHasher.GetNeighboursUnrolled,
	                                              b: GeoHasher.GetNeighboursArray,
	                                              p: s);

}
