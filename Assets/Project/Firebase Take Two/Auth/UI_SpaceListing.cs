using System;

using UnityEngine;
using UnityEngine.UI;

using Vanilla.Pools;

namespace Spatial
{

	[Serializable]
	public class UI_SpaceListing : PoolItem<SpaceListingPool, UI_SpaceListing>
	{

		public SpaceListing target;

		public Text name;
		public Text creator;

		public Text description;

		public Image lockIcon;

		public  Text             currentUserCount;


		
		public void Populate(SpaceListing target,
		                     Sprite lockSprite)
		{
			this.target = target;

			name.text             = target.dic["name"] as string;
			creator.text          = target.creatorName;
			description.text      = target.dic["description"] as string;
			currentUserCount.text = target.dic["current_users"].ToString();
			
			if (lockSprite == null)
			{
				lockIcon.gameObject.SetActive(false);
			}
			else
			{
				lockIcon.gameObject.SetActive(true);

				lockIcon.sprite = lockSprite;
			}
			
			gameObject.SetActive(true);
		}


		public void ToggleInteractable(bool state)
		{
			
		}


		protected internal override void OnGet()
		{
			
		}


		protected internal override void OnRetire()
		{
			
		}

		public override SpaceListingPool Pool
		{
			get => null;
			set { }
		}

	}

}