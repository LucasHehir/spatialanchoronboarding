using System;

using UnityEngine;

namespace Vanilla.Haversine
{

	public static class Haversine
	{

		public static double DistanceInMiles(double latA,
		                                     double longA,
		                                     double latB,
		                                     double longB) => 3960 *
		                                                      Distance(latA: latA,
		                                                               longA: longA,
		                                                               latB: latB,
		                                                               longB: longB);


		public static double DistanceInKilometres(double latA,
		                                          double longA,
		                                          double latB,
		                                          double longB) => 6371 *
		                                                           Distance(latA: latA,
		                                                                    longA: longA,
		                                                                    latB: latB,
		                                                                    longB: longB);


		private static double Distance(double latA,
		                               double longA,
		                               double latB,
		                               double longB)
		{
			var dLat = Mathf.Deg2Rad * (latB  - latA);
			var dLon = Mathf.Deg2Rad * (longB - longA);

			var latDiv  = Math.Sin(dLat / 2);
			var longDiv = Math.Sin(dLon / 2);

			return 2 *
			       Math.Asin(Math.Min(val1: 1,
			                          val2: Math.Sqrt(latDiv * latDiv +
			                                          Math.Cos(Mathf.Deg2Rad * latA) *
			                                          Math.Cos(Mathf.Deg2Rad * latB) *
			                                          longDiv                        *
			                                          longDiv)));

//			return 2 *
//			       Math.Asin(Math.Min(val1: 1,
//			                          val2: Math.Sqrt(Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
//			                                          Math.Cos(Mathf.Deg2Rad * latA) *
//			                                          Math.Cos(Mathf.Deg2Rad * latB) *
//			                                          Math.Sin(dLon          / 2)    *
//			                                          Math.Sin(dLon          / 2))));
		}

	}

}