//using System;
//
//namespace Vanilla.GeoYoink
//{
//
//    public class GeoYoinkLocation
//    {
//
//        /** The latitude of this location in the range of [-90, 90] */
//        public readonly double latitude;
//
//        /** The longitude of this location in the range of [-180, 180] */
//        public readonly double longitude;
//
//
//        /**
//         * Creates a new GeoLocation with the given latitude and longitude.
//         *
//         * @throws java.lang.IllegalArgumentException If the coordinates are not valid geo coordinates
//         * @param latitude The latitude in the range of [-90, 90]
//         * @param longitude The longitude in the range of [-180, 180]
//         */
//        public GeoYoinkLocation(double latitude,
//                                double longitude)
//        {
//            if (!coordinatesValid(latitude: latitude,
//                                  longitude: longitude))
//            {
//                throw new Exception("Not a valid geo location: " + latitude + ", " + longitude);
//            }
//
//            this.latitude  = latitude;
//            this.longitude = longitude;
//        }
//
//
//        /**
//         * Checks if these coordinates are valid geo coordinates.
//         * @param latitude The latitude must be in the range [-90, 90]
//         * @param longitude The longitude must be in the range [-180, 180]
//         * @return True if these are valid geo coordinates
//         */
//        public static bool coordinatesValid(double latitude,
//                                            double longitude) => latitude >= -90 && latitude <= 90 && longitude >= -180 && longitude <= 180;
//
//
//        public override bool Equals(object o)
//        {
//            if (this == o) return true;
//
//            if (o         == null ||
//                GetType() != o.GetType()) return false;
//
//            var that = (GeoYoinkLocation) o;
//
//            if (!Equals(objA: that.latitude,
//                        objB: latitude)) return false;
//
//            return Equals(objA: that.longitude,
//                          objB: longitude);
//        }
//
//
//        public override int GetHashCode()
//        {
//            var temp   = BitConverter.DoubleToInt64Bits(latitude);
//            var result = (int) (temp ^ (temp >> 32));
//            temp   = BitConverter.DoubleToInt64Bits(longitude);
//            result = 31 * result + (int) (temp ^ (temp >> 32));
//
//            return result;
//        }
//
//
//        public override string ToString() => "GeoLocation(" + latitude + ", " + longitude + ")";
//
//    }
//
//}