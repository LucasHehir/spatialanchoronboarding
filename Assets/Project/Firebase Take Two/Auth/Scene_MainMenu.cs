#if DEBUG_SPATIAL && SCENE_MAINMENU
#define debug
#endif

using System;
using System.Collections;
using System.Threading.Tasks;

using Cysharp.Threading.Tasks;

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using Vanilla.GeoHasher;
using Vanilla.Pools;
using Vanilla.ScreenFader;

using static UnityEngine.Debug;

namespace Spatial
{

	[Serializable]
	public class SpaceListingPool : Pool<SpaceListingPool, UI_SpaceListing> { } 
	
	[Serializable]
	public class Scene_MainMenu : MonoBehaviour
	{
		
		[Header("Spaces Listing")]

		[SerializeField]
		public SpaceListingPool listings = new SpaceListingPool();

		public int listingsHeightOffset = 128;

		public int listingHeight = 200;

		public Button createSpaceButton;
		
		public Button refreshListingsButton;

		public RectTransform refreshButtonIcon;

		public Sprite secureIcon;
		
		public double RangeInKM = 0.5f;

		[ContextMenu("Fill Listings Pool")]
		private void FillListingsPool() => listings.Fill();

		private IEnumerator SpinRefreshIcon()
		{
			var e = Vector3.zero;

			const float rate = -100.0f;

			while (true)
			{
				refreshButtonIcon.localEulerAngles = e;

				e.z += rate * Time.deltaTime;

				yield return null;
			}
		}

		public async void Refresh()
		{
			if (Spaces.Fetching)
			{
				LogWarning("Already updating before Refresh was hit? How did that happen?");

				return;
			}

			refreshListingsButton.interactable = false;

			for (var i = listings.InUse.Count - 1;
			     i >= 0;
			     i--)
			{
				listings.Retire(listings.InUse[i]);
			}

			var spin = SpinRefreshIcon();

			StartCoroutine(spin);

			#if debug
			Log("Refresh begun");
			#endif

			var (Lat, Long) = GeoHasher.HashToLatLong(input: Location.SessionGeohash.Substring(startIndex: 0,
			                                                                                   length: 8));
			
			var fetchResults = await Spaces.FetchNearbyListings(lat: Lat,
			                                                    lon: Long,
			                                                    maxRangeInKm: RangeInKM);

			#if debug
			Log("Refresh complete");
			#endif

			StopCoroutine(spin);

			foreach (var l in fetchResults)
			{
				listings.Get().Populate(target: l,
				                        lockSprite: (bool) l.dic["secure"] ? secureIcon : null);

				await Task.Yield();
			}

			var parentSize = ((RectTransform) listings.sceneParent).sizeDelta;

			parentSize.y = listingsHeightOffset + listingHeight * listings.InUse.Count;

			((RectTransform) listings.sceneParent).sizeDelta = parentSize;

			refreshListingsButton.interactable = true;

		}

		async void Start()
		{
			createSpaceButton.onClick.AddListener(ShowConfirmCreateWindow);
			refreshListingsButton.onClick.AddListener(Refresh);
			
			ScreenFader.FadeIn();

			var userSnapshot = await Users.CurrentUser.GetSnapshotAsync();

			if (!userSnapshot.Exists)
			{
				LogError($"Uh oh! We failed to find your User entry online. It should look something like this [{FirebaseAuth_Utility.GetEncryptedCurrentUserID()}]");

				return;
			}

			var userDic = userSnapshot.ToDictionary();

			var userName = (string) userDic["userName"];

			Log($"Welcome to Spatial [{userName}]");
			
			Refresh();
		}


		private void ShowConfirmCreateWindow()
		{
			// You can create 2 more spaces.

			// Are you sure want to create a new space?
			
			// Yes / No
			
			// ------------
			
			// You have already created the maximum number of spaces.
			
			// You can delete an old space by selecting it and choosing "Delete"
			// This can also be done from the "My Spaces" menu.
			
		}


		[ContextMenu("Sign Out")]
		public void SignOut()
		{
			FirebaseAuth_Utility.SignOut();

			Log("signed out");
		}


		[ContextMenu(itemName: "Navigate to Welcome scene")]
		public void NavigateToWelcomeScene()
		{
			FirebaseAuth_Utility.SignOut();

			SceneManager.LoadSceneAsync(sceneName: "Welcome",
			                            mode: LoadSceneMode.Additive);

			SceneManager.UnloadSceneAsync(scene: gameObject.scene);
		}




	}

}