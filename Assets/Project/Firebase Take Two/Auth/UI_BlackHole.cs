using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

using Cysharp.Threading.Tasks;

using UnityEngine;
using UnityEngine.UI;

using Vanilla.Easing;

public class UI_BlackHole : MonoBehaviour
{

	public RectTransform blackHole;

	public bool doBlackHoleSin = true;
	public bool inTransition   = false; 

	public float blackHoleSinRate = 0.5f;

	public float blackHoleSinMin = 0.5f;
	public float blackHoleSinMax = 1.0f;

	public float blackHoleFullScreenSize = 10.0f;
	public float blackHoleFullScreenSeconds = 0.5f;

	public Image blackHoleSprite;

	public Image background;
	
//	public Camera cam;

	public int depth = 0;

	[SerializeField]
	public MenuLayer[] menuLayers = new MenuLayer[3];
	
	[Serializable]
	public struct MenuLayer
	{

//		[SerializeField]
//		public GameObject ui;

		[SerializeField]
		public Color      background;

	}
	
	public void Start()
	{
		blackHole.localScale = new Vector3(x: blackHoleSinMin,
		                                   y: blackHoleSinMin,
		                                   z: blackHoleSinMin);
		
		StartBlackholeSin();
		
//		InvokeRepeating(ScaleBlackHoleUp, 1.0f, 1.0);
	}


	private void OnDestroy() => doBlackHoleSin = false;


	[ContextMenu(itemName: "Start BlackHole Sin")]
	public async UniTask StartBlackholeSin()
	{
		doBlackHoleSin = true;

		var t = -1.5f;

		while (doBlackHoleSin)
		{
			t += Time.deltaTime * blackHoleSinRate;

			var scale = Mathf.Lerp(a: blackHoleSinMin,
			                       b: blackHoleSinMax,
			                       t: 0.5f + Mathf.Sin(f: t) * 0.5f);

			blackHole.localScale = new Vector3(x: scale,
			                                   y: scale,
			                                   z: scale);

			await UniTask.Yield();

		}
	}



	[ContextMenu(itemName: "Scale BlackHole Up")]
	public async UniTask ScaleBlackHoleUp()
	{
		if (inTransition || depth > menuLayers.Length - 3) return;

		inTransition   = true;
		
		doBlackHoleSin = false;
		
		var t = 0.0f;

		var rate = 1.0f / blackHoleFullScreenSeconds;

		var startScale = blackHole.localScale.x;

		while (t < 1.0f)
		{
			t += Time.deltaTime * rate;

			var scale = Mathf.Lerp(a: startScale,
			                       b: blackHoleFullScreenSize,
			                       t: Easing.InPower(t: t,
			                                            power: 4.0f));

			blackHole.localScale = new Vector3(x: scale,
			                                   y: scale,
			                                   z: scale);

			await UniTask.Yield();
		}

//		menuLayers[depth].ui.gameObject.SetActive(false);

		

//		if (depth < menuLayers.Length - 2)
//		{
			depth++;
//		}

		background.color    = menuLayers[depth].background;
//		cam.backgroundColor = menuLayers[depth].background;

		blackHoleSprite.color = menuLayers[depth+1].background;
		
//		menuLayers[depth].ui.gameObject.SetActive(true);
		
		startScale = 0.0f;

		t = 0.0f;
		
		while (t < 1.0f)
		{
			t += Time.deltaTime * rate;

			var scale = Mathf.Lerp(a: startScale,
			                       b: blackHoleSinMin,
			                       t: Easing.OutPower(t: t,
			                                            power: 4.0f));

			blackHole.localScale = new Vector3(x: scale,
			                                   y: scale,
			                                   z: scale);

			await UniTask.Yield();
		}
		
		inTransition = false;

		doBlackHoleSin = true;

		StartBlackholeSin();
	}
	
	[ContextMenu(itemName: "Scale BlackHole Down")]
	public async UniTask ScaleBlackHoleDown()
	{
		if (inTransition || depth < 1) return;

		inTransition = true;
		
		doBlackHoleSin = false;

		var t = 0.0f;

		var rate = 1.0f / blackHoleFullScreenSeconds;

		var startScale = blackHole.localScale.x;

		while (t < 1.0f)
		{
			t += Time.deltaTime * rate;

			var scale = Mathf.Lerp(a: startScale,
			                       b: 0.0f,
			                       t: Easing.InPower(t: t,
			                                         power: 4.0f));

			blackHole.localScale = new Vector3(x: scale,
			                                   y: scale,
			                                   z: scale);

			await UniTask.Yield();
		}
		
//		menuLayers[depth].ui.gameObject.SetActive(false);

//		if (depth > 1)
//		{
			depth--;
//		}

		
		background.color    = menuLayers[depth].background;
//		cam.backgroundColor = menuLayers[depth].background;

		blackHoleSprite.color = menuLayers[depth + 1].background;
		
//		menuLayers[depth].ui.gameObject.SetActive(true);
		
		t = 0.0f;
		
		while (t < 1.0f)
		{
			t += Time.deltaTime * rate;

			var scale = Mathf.Lerp(a: blackHoleFullScreenSize,
			                       b: blackHoleSinMin,
			                       t: Easing.OutPower(t: t,
			                                          power: 4.0f));

			blackHole.localScale = new Vector3(x: scale,
			                                   y: scale,
			                                   z: scale);

			await UniTask.Yield();
		}
		
		inTransition = false;

		doBlackHoleSin = true;

		StartBlackholeSin();
	}

}