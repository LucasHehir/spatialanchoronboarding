using System;

using Cysharp.Threading.Tasks;

using UnityEngine;

using Firebase.Auth;
using Firebase.Firestore;

using Vanilla.StringFormatting;

using static UnityEngine.Debug;

public static class FirebaseAuth_Utility
{

	// -------------------------------------------------------------------------------------------------------------------------------- Constants //

	internal const string c_SignInCache_Username_Key = "SignInCache_Username";

	internal const string c_SignInCache_Email_Key = "SignInCache_Email";

	internal const string c_SignInCache_Password_Key = "SignInCache_Password";

	// ----------------------------------------------------------------------------------------------------------------------- Account Management //

	public static async UniTask<bool> CreateAccount(string email,
	                                                string password) => await FirebaseAuth.DefaultInstance.CreateUserWithEmailAndPasswordAsync(email: email,
	                                                                                                                                           password: password).ContinueWith(continuationFunction: task => !(task.IsCanceled || task.IsFaulted));


	public static async UniTask<bool> SignIn(string email,
	                                         string password) => await FirebaseAuth.DefaultInstance.SignInWithEmailAndPasswordAsync(email: email,
	                                                                                                                                password: password).ContinueWith(continuationFunction: task => !(task.IsCanceled || task.IsFaulted));
	
	public static void SignOut() => FirebaseAuth.DefaultInstance.SignOut();


	public static void DeleteSignInCache()
	{
		if (PlayerPrefs.HasKey(key: c_SignInCache_Username_Key))
		{
			PlayerPrefs.DeleteKey(key: c_SignInCache_Username_Key);
		}

		if (PlayerPrefs.HasKey(key: c_SignInCache_Email_Key))
		{
			PlayerPrefs.DeleteKey(key: c_SignInCache_Email_Key);
		}

		if (PlayerPrefs.HasKey(key: c_SignInCache_Password_Key))
		{
			PlayerPrefs.DeleteKey(key: c_SignInCache_Password_Key);
		}
	}
	
	// ----------------------------------------------------------------------------------------------------------------- Get Current User Details //

	public static void LogCurrentUserDetails() => Log(message: $"Firebase User Details\n\tEmail\t\t\t{FirebaseAuth.DefaultInstance.CurrentUser.Email}\n\tDisplay Name\t\t{FirebaseAuth.DefaultInstance.CurrentUser.DisplayName}\n\tAnonymous?\t\t{FirebaseAuth.DefaultInstance.CurrentUser.IsAnonymous}\n\tPhone number\t\t{FirebaseAuth.DefaultInstance.CurrentUser.PhoneNumber}\n\tUserId\t\t\t{FirebaseAuth.DefaultInstance.CurrentUser.UserId}\n\tAccount Metadata\n\t\tCreation date\t{new DateTime(ticks: (long) FirebaseAuth.DefaultInstance.CurrentUser.Metadata.CreationTimestamp)}\n\t\tLast login\t\t{new DateTime(ticks: (long) FirebaseAuth.DefaultInstance.CurrentUser.Metadata.LastSignInTimestamp)}");

	public static DocumentReference GetCurrentUserDocument() => FirebaseFirestore.DefaultInstance.Document("users/" + GetEncryptedCurrentUserID());
	
	public static string GetCurrentUserID() => FirebaseAuth.DefaultInstance.CurrentUser.UserId;

	public static string GetEncryptedCurrentUserID() => GetCurrentUserID().Xor(2147168641);

	public static string GetCurrentUserUserName() => PlayerPrefs.GetString(key: c_SignInCache_Username_Key).Xor(key: SystemInfo.deviceUniqueIdentifier.GetHashCode());

}