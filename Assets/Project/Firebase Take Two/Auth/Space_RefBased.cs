using System;
using System.Collections;
using System.Collections.Generic;

using Firebase.Extensions;
using Firebase.Firestore;

using UnityEngine;

using static UnityEngine.Debug;

[Serializable]
public class Space_RefBased : MonoBehaviour
{
	
	// Rather than pulling the data down all the time so its local (which isn't necessarily a bad idea by itself)
	
	// It seems that with Firestore, you can reconstruct the layout of your cloud data locally using DocumentReference
	// and CollectionReference.
	
	// Initializing them doesn't actually perform any network operations, it's all just skeletal endpoints that you can
	// then tap as needed. Very cool!

	// Let's try this out for Spaces - it looks like the root of a Space is a... document?

	public static CollectionReference spaces;
	
	public DocumentReference template;

	public CollectionReference beacons;

	public CollectionReference cloud_anchors;

	public DocumentReference you_can_also_do_this;

	public string spaceName;
	
	void Awake()
	{
		
	}

	public async void Start()
	{
		spaces = FirebaseFirestore.DefaultInstance.Collection(path: "spaces");
		
		template  = spaces.Document(path: "template");

		beacons = template.Collection(path: "beacons");

		cloud_anchors = template.Collection(path: "cloud_anchors");

//		await template.GetSnapshotAsync().ContinueWithOnMainThread(continuation: task =>
//		                                                                         {
//			                                                                         var snapshot = task.Result;
//
//			                                                                         if (!snapshot.Exists)
//			                                                                         {
//				                                                                         LogWarning(message: "No dice");
//
//				                                                                         return;
//			                                                                         }
//
//			                                                                         var dic = snapshot.ToDictionary();
//
//			                                                                         Log(message: (string) dic[key: "name"]);
//		                                                                         });

//		you_can_also_do_this = FirebaseFirestore.DefaultInstance.Document("spaces/template/beacons/template");

//		var snapshot = await you_can_also_do_this.GetSnapshotAsync();

//		if (snapshot.Exists)
//		{
//			snapshot.ConvertTo<>()
	}


	void Update()
	{
		if (!Input.GetKeyDown(key: KeyCode.Space)) return;

		UpdateRoomName();
	}

	public async void UpdateRoomName() => await template.GetSnapshotAsync().ContinueWithOnMainThread(continuation: task =>
	                                                                                                               {
		                                                                                                               var snapshot = task.Result;

		                                                                                                               if (!snapshot.Exists)
		                                                                                                               {
			                                                                                                               LogWarning(message: "No dice");

			                                                                                                               return;
		                                                                                                               }

		                                                                                                               var dic = snapshot.ToDictionary();

		                                                                                                               spaceName = dic[key: "name"] as string;
	                                                                                                               });


	//		if (snapshot.Query.

//		you_can_also_do_this.GetSnapshotAsync().ContinueWithOnMainThread(task =>
//		                                                                 {
//			                                                                 var snapshot = task.Result;
//
//			                                                                 if (snapshot.Exists)
//			                                                                 {
//				                                                                 Log($"Fetch successful for [{snapshot.Id}]");
//
//				                                                                 testRoom = snapshot.ConvertTo<Space>();
//
//				                                                                 foreach (var pair in snapshot.ToDictionary())
//				                                                                 {
//					                                                                 Log($"{pair.Key}\t\t{pair.Value}");
//				                                                                 }
//			                                                                 }
//			                                                                 else
//			                                                                 {
//				                                                                 Log($"Document {snapshot.Id} does not exist!");
//			                                                                 }
//		                                                                 });

}
