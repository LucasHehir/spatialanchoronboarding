using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Cysharp.Threading.Tasks;

using Firebase.Extensions;
using Firebase.Firestore;

using UnityEngine;

using Vanilla.GeoHasher;
using Vanilla.Haversine;

using static UnityEngine.Debug;

//public static partial class Spatial
//{

namespace Spatial
{

	public static class Spaces
	{

		private const string c_SpaceHashes_ID = "space_hashes";

		private static bool _fetching = false;
		public static  bool Fetching => _fetching;

//		public static bool onceOnlyPlz = false;
		
		private static IOrderedEnumerable<SpaceListing> _currentSpaces;
		public static  IOrderedEnumerable<SpaceListing> CurrentSpaces => _currentSpaces;

		//		public static 


		public static async Task<DocumentReference> Create(string name,
		                                                   string description,
		                                                   double latitude,
		                                                   double longitude)
		{
			var hash = GeoHasher.LatLongToHash(latitude: latitude,
			                                   longitude: longitude,
			                                   precision: 12);

			var newDocRef = FirebaseFirestore.DefaultInstance.Collection(path: $"{c_SpaceHashes_ID}/{hash[0]}/{hash[1]}{hash[2]}/{hash[3]}{hash[4]}/{hash[5]}{hash[6]}").Document();

			var newSpace = new Dictionary<string, object>
			               {
				               { "name", name },
				               { "description", description },
				               { "creator", FirebaseFirestore.DefaultInstance.Document("users/" + FirebaseAuth_Utility.GetEncryptedCurrentUserID()) },
				               { "geo_hash", hash },
				               {
					               "location", new GeoPoint(latitude: latitude,
					                                        longitude: longitude)
				               }

//				               { "anchor_ids", newDocRef.Collection("cloud_anchors") },
//				               { "beacons", newDocRef.Collection("beacons") }
			               };

			await newDocRef.SetAsync(documentData: newSpace).ContinueWithOnMainThread(continuation: task => task);

			Log(message: $"New Space [{newDocRef.Id}] created");

			return newDocRef;
		}


		public static async Task CreateSpaceGeohashReference(string geohash)
		{
			var builder = new StringBuilder(32);

			builder.Append("space_hashes");

			foreach (var c in geohash)
			{
				builder.Append('/');
				builder.Append(c);
			}

//			var hashRefString = "space_hashes"

//			var hashRef = FirebaseFirestore.DefaultInstance.Collection("space_hashes/r/1/r/1/1/4/7/f");

			var hashRef = FirebaseFirestore.DefaultInstance.Collection(builder.ToString());

			Log(hashRef.Id);



		}
		
//
//
//		public static async Task<IOrderedEnumerable<SpaceListing>> GetSpacesFromLatLong(double lat,
//		                                                                                double lon)
//		{
//			// Turn our lat/long into a Geohash
//
//			var hash = GeoHasher.LatLongToHash(latitude: lat,
//			                                   longitude: lon);
//
//			// Fetch the neighbouring cells to this cell
//
////            var proximity = GeoQueryProximity.CloseBy;
//
//			var neighbours = GeoHasher.GetNeighboursUnrolled(geohash: hash);
//
//			// The neighbours are contained in an array starting with the north-west cell
//			// iterating eastwards followed by downwards (i.e nw, n, ne, w, e, sw, s, se)
//
//			var results = new List<SpaceListing>();
//
//			foreach (var n in neighbours)
//			{
//				// Firestore hierarchy works in an alternating fashion between 'collections' and 'documents'
//				// i.e. collection.document.collection.document.etc
//
////                switch (proximity)
////                {
////                    case GeoQueryProximity.Accurate:
//
//				var hashID = $"{c_SpaceHashes_ID}/{n[0]}/{n[1]}{n[2]}/{n[3]}{n[4]}/{n[5]}{n[6]}";
//
//				Log($"Searching hash index [{hashID}]");
//
//				var accurateRef = FirebaseFirestore.DefaultInstance.Collection(hashID);
//
////                accurateRef.Document()
//
//				var accurate = await accurateRef.GetSnapshotAsync();
//
//				Log($"Docs found [{accurate.Documents.Count()}]");
//
//				foreach (var d in accurate.Documents)
//				{
//					// This works!!!
//
//					// It's just that the spaces have to live in the depths of the space_hashes collections...
//					// If you ever needed to iterate over all the spaces, you'd be kinda fucked.
//
//					// Let's try again with this ref mode and compare time taken/performance. Maybe its not that bad?
//
////                    var spaceDoc = await d.Reference.GetSnapshotAsync();
////
////                    var space = spaceDoc.ToDictionary();
////
////                    Log(space["name"]);
////
////                    var spacePoint = (GeoPoint) space["location"];
////
////                    var distInKm = Haversine.DistanceInKilometres(latA: lat,
////                                                                  longA: lon,
////                                                                  latB: spacePoint.Latitude,
////                                                                  longB: spacePoint.Longitude);
////                    
////                    if (distInKm < 1.0f)
////                    {
////	                    Log($"[{space["name"]}] is within range!");
////
////	                    results.Add(new Space_Listing(docRef: d.Reference,
////	                                                  dic: space,
////	                                                  distInKm: distInKm));
////                    }
////                    else
////                    {
////	                    Log($"[{space["name"]}] is too far away...");
////                    }
//
//					//-----------------------------------------------------------------------------------------------
//
//					// This works for referencing spaces seperately!
//
//					if (d.Id != "template") continue;
//
//					var spaceDoc = await d.Reference.GetSnapshotAsync();
//
////
//					var space = spaceDoc.ToDictionary();
//
//					var spaceRef = space["space"] as DocumentReference;
//
////                    Log(spaceRef.ToString());
//
////                    Log(space["dumb"]);
//
//					var spaceRefSnapshot = await spaceRef.GetSnapshotAsync();
//
//					var spaceRefSnapshotDic = spaceRefSnapshot.ToDictionary();
//
//					var spacePoint = (GeoPoint) spaceRefSnapshotDic["location"];
//
//					var distInKm = Haversine.DistanceInKilometres(latA: lat,
//					                                              longA: lon,
//					                                              latB: spacePoint.Latitude,
//					                                              longB: spacePoint.Longitude);
//
//					if (distInKm < 1.0f)
//					{
//						Log($"[{spaceRefSnapshotDic["name"]}] is within range!");
//
//						results.Add(new SpaceListing(docRef: d.Reference,
//						                             dic: spaceRefSnapshotDic,
//						                             distInKm: distInKm));
//					}
//					else
//					{
//						Log($"[{spaceRefSnapshotDic["name"]}] is too far away...");
//					}
//
//					//---------------------------------------------------------------------------------------------
//
//				}
//
//			}
//
//			return results.OrderBy(i => i.distInKm);
//		}
//

//		public static async Task FetchNearbyListings(double lat,
//		                                                                               double lon,
//		                                                                               double maxRangeInKm = 1.0f)
		public static async Task<IOrderedEnumerable<SpaceListing>> FetchNearbyListings(double lat,
		                                                                               double lon,
		                                                                               double maxRangeInKm = 1.0f)
		{
//			if (onceOnlyPlz)
//			{
//				LogWarning("I knew it!!");
//
//				return null;
//			}
			
			if (_fetching)
			{
				LogWarning("Update already in progress; hold your horses!!");

				return null;
			}
			
			_fetching = true;

			// Turn our lat/long into a Geohash

			var fullHash = GeoHasher.LatLongToHash(latitude: lat,
			                                   longitude: lon);

			var shortHash = fullHash.Substring(startIndex: 0,
			                               length: 7);

			// Fetch the neighbouring cells to this cell and list all the rooms within it
			// This gets around the possibility that we might be at the edge of a cell,
			// and thus not shown spaces near us but still shown results further away

			var neighbours = GeoHasher.GetNeighboursUnrolled(geohash: shortHash);

			// The neighbours are contained in an array starting with the north-west cell
			// iterating eastwards followed by downwards (i.e nw, n, ne, w, e, sw, s, se)

			Log($"Neighbour Map\n\n[NW]\t{neighbours[0]}\t[N]\t{neighbours[1]}\t[NE]\t{neighbours[2]}\n[W]\t{neighbours[3]}\t[C]\t{neighbours[4]}\t[E]\t{neighbours[5]}\n[SW]\t{neighbours[6]}\t[S]\t{neighbours[7]}\t[SE]\t{neighbours[8]}\n");

			var results = new List<SpaceListing>();

			foreach (var n in neighbours)
			{
				// Firestore hierarchy works in an alternating fashion between 'collections' and 'documents'
				// i.e. collection.document.collection.document.etc

				var hashID = $"{c_SpaceHashes_ID}/{n[0]}/{n[1]}{n[2]}/{n[3]}{n[4]}/{n[5]}{n[6]}";

				Log($"Fetching geohash collection at [{hashID}]");

				var hashCollection = await FirebaseFirestore.DefaultInstance.Collection(hashID).GetSnapshotAsync();

				Log($"Docs found [{hashCollection.Documents.Count()}]");

				foreach (var d in hashCollection.Documents)
				{
					var space = d.ToDictionary();

//	                var spaceSnapshot = await ((DocumentReference) space["space"]).GetSnapshotAsync();

//                    var spaceDic = spaceSnapshot.ToDictionary();

					var spacePoint = (GeoPoint) space["geo_point"];

					var distToSpace = Haversine.DistanceInKilometres(latA: lat,
					                                                 longA: lon,
					                                                 latB: spacePoint.Latitude,
					                                                 longB: spacePoint.Longitude);

					if (distToSpace < maxRangeInKm)
					{
						Log($"[{space["name"]}] is within range!");

						var creatorDoc = await ((DocumentReference) space["creator"]).GetSnapshotAsync();

						var creatorDic = creatorDoc.ToDictionary();
						
						results.Add(item: new SpaceListing(docRef: d.Reference,
						                                   dic: space,
						                                   creatorName: creatorDic["userName"] as string,
						                                   distInKm: distToSpace));
					}
					else
					{
						Log($"[{space["name"]}] is too far away...");
					}

				}

			}

			_fetching = false;

//			onceOnlyPlz = true;

//			_currentSpaces = results.OrderBy(i => i.distInKm);

//			return _currentSpaces;
			
			return results.OrderBy(i => i.distInKm);
			
		}

	}

	[Serializable]
	public class SpaceListing
	{

		public DocumentReference docRef;

		public Dictionary<string, object> dic;

		public string creatorName;

		public double distInKm;


		public SpaceListing(DocumentReference docRef,
		                    Dictionary<string, object> dic,
		                    string creatorName,
		                    double distInKm)
		{
			this.docRef      = docRef;
			this.dic         = dic;
			this.creatorName = creatorName;
			this.distInKm    = distInKm;
		}

	}

}