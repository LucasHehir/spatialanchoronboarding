using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

using Cysharp.Threading.Tasks;

using Firebase.Auth;
using Firebase.Extensions;
using Firebase.Firestore;

using UnityEngine;

using static UnityEngine.Debug;

public class FirebaseExperiments : MonoBehaviour
{

	[Header(header: "Room testing")]
	[SerializeField]
	public Space testRoom;


	[FirestoreData]
	public class RoomAccess
	{

		[FirestoreProperty]
		public long role
		{
			get;
			set;
		}

		[FirestoreProperty]
		public string room
		{
			get;
			set;
		}

//		[FirestoreProperty]
//		public DocumentReference room
//		{
//			get;
//			set;
//		}

	}

	// /users/ujqX2ftbGtbYWJZDm982ZWVGFTl1

//	[FirestoreData]
//	[Serializable]
//	public class SpatialUser { }

	[ContextMenu(itemName: "Debug - Get Room Template")]
	public async Task DebugFetchRoomTemplate()
	{
		var docRef = FirebaseFirestore.DefaultInstance.Collection(path: "rooms").Document(path: "template");

		await docRef.GetSnapshotAsync().ContinueWithOnMainThread(continuation: task =>
		                                                                       {
			                                                                       var snapshot = task.Result;

			                                                                       if (snapshot.Exists)
			                                                                       {
				                                                                       Log(message: $"Fetch successful for [{snapshot.Id}]");

				                                                                       testRoom = snapshot.ConvertTo<Space>();

				                                                                       foreach (var pair in snapshot.ToDictionary())
				                                                                       {
					                                                                       Log(message: $"{pair.Key}\t\t{pair.Value}");
				                                                                       }
			                                                                       }
			                                                                       else
			                                                                       {
				                                                                       Log(message: $"Document {snapshot.Id} does not exist!");
			                                                                       }
		                                                                       });
	}


	[ContextMenu(itemName: "Debug - Set Room")]
	public async Task DebugSetRoom()
	{
		var roomCollection = FirebaseFirestore.DefaultInstance.Collection(path: "rooms");

		// This reference will have an ID automatically generated for us.
		// You can get that ID immediately with newRoomDoc.Id
		var newRoomDoc = roomCollection.Document();

		Log(message: newRoomDoc.Id);

		var newRoomData = new Dictionary<string, object>
		                  {
			                  {
				                  "anchor_ids", new[]
				                                {
					                                "AAA",
					                                "BBB",
					                                "CCC"
				                                }
			                  },
			                  {
				                  "location", new GeoPoint(latitude: 1,
				                                           longitude: 2)
			                  }
		                  };

		await newRoomDoc.SetAsync(documentData: newRoomData).ContinueWithOnMainThread(continuation: task =>
		                                                                                            {
			                                                                                            if (task.IsCanceled)
			                                                                                            {
				                                                                                            LogError(message: "[Set Room] was canceled.");

				                                                                                            return;
			                                                                                            }

			                                                                                            if (task.IsFaulted)
			                                                                                            {
				                                                                                            LogError(message: "[Set Room] encountered an error: " + task.Exception);

				                                                                                            return;
			                                                                                            }
		                                                                                            });
	}


	[ContextMenu(itemName: "Debug - Set User Details")]
	public async Task SetUserDetails()
	{

		var userDoc = FirebaseFirestore.DefaultInstance.Collection(path: "users").Document(path: FirebaseAuth.DefaultInstance.CurrentUser.UserId);

		var userData = new Dictionary<string, object>
		               {
			               {
				               "room_access", new[]
				                              {
					                              new RoomAccess
					                              {
						                              role = 0,
						                              room = "/rooms/template"
					                              },
					                              new RoomAccess
					                              {
						                              role = 1,
						                              room = "/rooms/test"
					                              }
				                              }
			               }
		               };

		await userDoc.SetAsync(documentData: userData).ContinueWithOnMainThread(continuation: task =>
		                                                                                      {
			                                                                                      if (task.IsCanceled)
			                                                                                      {
				                                                                                      LogError(message: "SetAsync was canceled.");

				                                                                                      return;
			                                                                                      }

			                                                                                      if (task.IsFaulted)
			                                                                                      {
				                                                                                      LogError(message: "SetAsync encountered an error: " + task.Exception);

				                                                                                      return;
			                                                                                      }

			                                                                                      Log(message: "Set... user... I think?");
		                                                                                      });
	}

	// -------------------------------------------------------------------------------------------------------------------------- Account Details //


//    public async Task Whatever()
//    {
//        var newProfile = new UserProfile
//                         {
//                             DisplayName = "Farty joe",
//                             PhotoUrl    = new Uri("https://www.google.com/")
//                         };
//
//
//        await FirebaseAuth.DefaultInstance.CurrentUser.UpdateUserProfileAsync(profile: new UserProfile
//                                                                                       {
//                                                                                           DisplayName = "Farty joe",
//                                                                                           PhotoUrl    = new Uri("https://www.google.com/")
//                                                                                       });
//    }

//	public IEnumerator PopulateUI()
//	{
//		var currentUser = FirebaseAuth.DefaultInstance.CurrentUser;
//
//		emailText.text       = currentUser.Email;
//		displayNameText.text = currentUser.DisplayName;
//		phoneText.text       = currentUser.PhoneNumber;
//		
////		Log(currentUser.PhotoUrl); // This is null. I guess you have to upload your own somewhere... 
//
////		var request = UnityWebRequestTexture.GetTexture(currentUser.PhotoUrl);
//
////		var what = UnityWebRequestTexture.GetTexture()
//
////		yield return request.SendWebRequest();
//
////		Log(request.result.ToString());
//
////		if (request.result != UnityWebRequest.Result.Success)
////		{
////			image.sprite = Sprite.Create(texture: ((DownloadHandlerTexture) request.downloadHandler).texture,
////			                             rect: Rect.zero,
////			                             pivot: Vector2.one * 0.5f,
////			                             pixelsPerUnit: 1.0f);
////		}
//
////		Log("user image F");
//
//		yield break;
//	}

}