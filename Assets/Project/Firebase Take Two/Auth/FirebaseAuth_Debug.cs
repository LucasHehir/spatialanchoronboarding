using UnityEngine;

public class FirebaseAuth_Debug : MonoBehaviour
{

	public string email;
	public string password;

	[ContextMenu(itemName: "Create Account")]
	private void CreateAccount() => FirebaseAuth_Utility.CreateAccount(email: email,
	                                                                password: password);


	[ContextMenu(itemName: "Sign In")]
	private void SignIn() => FirebaseAuth_Utility.SignIn(email: email,
	                                                  password: password);


	[ContextMenu(itemName: "Sign Out")]
	private void SignOut() => FirebaseAuth_Utility.SignOut();


	[ContextMenu(itemName: "Log Current User")]
	private void LogCurrentUserDetails() => FirebaseAuth_Utility.LogCurrentUserDetails();


	[ContextMenu(itemName: "Delete Sign In Cache")]
	private void DeleteSignInCache() => FirebaseAuth_Utility.DeleteSignInCache();

}