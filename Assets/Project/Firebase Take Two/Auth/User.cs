using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Firebase.Extensions;
using Firebase.Firestore;

using UnityEngine;

using Vanilla.GeoHasher;

//[FirestoreData]
//[Serializable]
//public class User
//{
//
//	[SerializeField]
//	private string _userName;
//
//	[FirestoreProperty]
//	public string userName
//	{
//		get => _userName;
//		set => _userName = value;
//	}
//	
//	
//
//}

namespace Spatial
//public static partial class Spatial
{

	public static class Users
	{

		// You can only refer to this after logging in (happens automatically after creating an account)
		// This is because the DocumentReference uses the FirebaseAuth key to map the current user to their Firestore user record
		
		private static DocumentReference _CurrentUser;
		public static  DocumentReference CurrentUser = _CurrentUser ??= FirebaseAuth_Utility.GetCurrentUserDocument();


		public static async Task Create(string userName)
		{
//			var newUserDoc = Users.Current;

//			var newUserDoc = FirebaseFirestore.DefaultInstance.Collection(path: "users").Document(path: id);

//			var newUser = new Dictionary<string, object>
//			              {
//				              { "userName", userName },
//				              { "completed_tutorial", false },
//				              {
//					              "favorite_spaces", new[]
//					                                  {
//						                                  FirebaseFirestore.DefaultInstance.Document(path: "spaces/template"),
//						                                  FirebaseFirestore.DefaultInstance.Document(path: "spaces/jvrHJLDHu7QRt8VDKQTd"),
//					                                  }
//				              }
//			              };

			var newUser = new Dictionary<string, object>
			              {
				              { "userName", userName },
				              { "completed_tutorial", false },
				              { "favorite_spaces", new DocumentReference[0] },
			              };

//			await CurrentUser.SetAsync(documentData: newUser);
			await CurrentUser.SetAsync(documentData: newUser).ContinueWithOnMainThread(continuation: task => task);

			Debug.Log(message: $"New User document created successfully [{CurrentUser.Id}]");
		}


		public static async Task SignIn() => CurrentUser = FirebaseFirestore.DefaultInstance.Document("users/" + FirebaseAuth_Utility.GetEncryptedCurrentUserID());

	}

}