using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

using Firebase.Auth;

using UnityEngine;

using Vanilla.GeoHasher;
using Vanilla.Haversine;
using Vanilla.StringFormatting;

using static UnityEngine.Debug;

public class GeoYoink_Debug : MonoBehaviour
{

	public double latitude = -37.780120;

	public double longitude = 144.892915;
	
	public double latitudeB;

	public double longitudeB;
	
	public int precision = 10;

	public string hash;

	public string a;
	public string b;
	public string c;
	
	public int key;

	[Header("Testing GetNeighbours")]
	public string neighbour_Input = "r1r1147f";

	[ContextMenu(itemName: "Create Test Space")]
	public async void CreateTestSpace()
	{
		var newSpaceDoc = await Spatial.Spaces.Create(name: "Something Stupid",
		                                              description: "What a lovely place",
		                                              latitude: latitude,
		                                              longitude: longitude);

	}


	[ContextMenu(itemName: "Create Test User")]
	public async void CreateTestUser() => await Spatial.Users.Create(userName: FirebaseAuth_Utility.GetCurrentUserUserName());


	void Update()
	{
		if (!Input.GetKeyDown(KeyCode.Space)) return;

		b = a.Xor(key);
		c = b.Xor(key);
	}


	[ContextMenu("Test Haversine")]
	public void TestHaversine() => Log(Haversine.DistanceInKilometres(latA: latitude,
	                                                                  longA: longitude,
	                                                                  latB: latitudeB,
	                                                                  longB: longitudeB));


	[ContextMenu(itemName: "Do it")]
	private void Doit() => hash = GeoHasher.LatLongToHash(latitude: latitude,
	                                                  longitude: longitude,
	                                                  precision: precision);


	[ContextMenu("Test GetNeighbours")]
	private void Test_GetNeighbours()
	{
//		var neighbours = GeoHasher.GetNeighboursDict(neighbour_Input);
		
//		Log($"Neighbour Map\n\n{neighbours["nw"]}\t{neighbours["n"]}\t{neighbours["ne"]}\n{neighbours["w"]}\t{neighbour_Input}\t{neighbours["e"]}\n{neighbours["sw"]}\t{neighbours["s"]}\t{neighbours["se"]}\n");
	}
	
//
//	[ContextMenu(itemName: "Race")]
//	private async void Race()
//	{
//		
//		GeoHasher.Calculate(latitude: latitude,
//		                    longitude: longitude,
//		                    precision: precision);
//
//		GeoHasher.Calculate2(latitude: latitude,
//		                     longitude: longitude,
//		                     precision: precision);
//
//		var a = new long[100];
//		var b = new long[100];
//		
//		var w = new Stopwatch();
//		
//		for (var i = 0;
//		     i < 100;
//		     i++)
//		{
//			w.Restart();
//			
//			GeoHasher.Calculate(latitude: latitude,
//			                    longitude: longitude,
//			                    precision: precision);
//
//			w.Stop();
//			
//			a[i] = w.ElapsedTicks;
//		}
//		
//		for (var i = 0;
//		     i < 100;
//		     i++)
//		{
//			w.Restart();
//			
//			GeoHasher.Calculate2(latitude: latitude,
//			                    longitude: longitude,
//			                    precision: precision);
//
//			w.Stop();
//			
//			b[i] = w.ElapsedTicks;
//		}

//		Log(message: $"Calcuate took {a.Average()} ticks on average");
//		Log(message: $"Calcuate2 took {b.Average()} ticks on average");
//	}
	
}
