using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenFader2 : MonoBehaviour
{

    private                 Material mat;
    
    private static readonly int      SrcBlend = Shader.PropertyToID("_SrcBlend");
    private static readonly int      DstBlend = Shader.PropertyToID("_DstBlend");
    private static readonly int      Cull     = Shader.PropertyToID("_Cull");
    private static readonly int      ZWrite   = Shader.PropertyToID("_ZWrite");
    private static readonly int      ZTest    = Shader.PropertyToID("_ZTest");


    void Start()
    {
	    
    }

    // Will be called from camera after regular rendering is done.
    public void OnPostRender()
    {
	    Debug.Log("on post render!");
	    
        if (!mat)
        {
            // Unity has a built-in shader that is useful for drawing
            // simple colored things. In this case, we just want to use
            // a blend mode that inverts destination colors.
            var shader = Shader.Find("Hidden/Internal-Colored");

            mat = new Material(shader)
                  {
	                  hideFlags = HideFlags.HideAndDontSave
                  };

            // Set blend mode to invert destination colors.
            mat.SetInt(nameID: SrcBlend,
                       value: (int) UnityEngine.Rendering.BlendMode.OneMinusDstColor);

            mat.SetInt(nameID: DstBlend,
                       value: (int) UnityEngine.Rendering.BlendMode.Zero);

            // Turn off backface culling, depth writes, depth test.
            mat.SetInt(nameID: Cull,
                       value: (int) UnityEngine.Rendering.CullMode.Off);

            mat.SetInt(nameID: ZWrite,
                       value: 0);

            mat.SetInt(nameID: ZTest,
                       value: (int) UnityEngine.Rendering.CompareFunction.Always);
        }

        GL.PushMatrix();
        GL.LoadOrtho();

        // activate the first shader pass (in this case we know it is the only pass)
        mat.SetPass(0);

        // draw a quad over whole screen
        GL.Begin(GL.QUADS);

        GL.Vertex3(x: 0,
                   y: 0,
                   z: 0);

        GL.Vertex3(x: 1,
                   y: 0,
                   z: 0);

        GL.Vertex3(x: 1,
                   y: 1,
                   z: 0);

        GL.Vertex3(x: 0,
                   y: 1,
                   z: 0);

        GL.End();

        GL.PopMatrix();
    }

}