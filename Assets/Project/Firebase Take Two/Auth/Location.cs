#if DEBUG_SPATIAL
#define debug
#endif

using Cysharp.Threading.Tasks;

using UnityEngine;

using Vanilla.GeoHasher;

using static UnityEngine.Debug;

namespace Spatial
{

    public static class Location
    {
        
        private static string _sessionGeohash;
        public static string SessionGeohash => _sessionGeohash;
        
        public static async UniTask<bool> Start()
        {
            if (!Input.location.isEnabledByUser) return false;

            Input.location.Start();

            var timeout = 0;

            // Check in every half-second for 10 seconds - 'time out' if it takes longer than that.
            // Also my first practical do-while loop :D

            do
            {
                await UniTask.Delay(millisecondsDelay: 500);

                if (++timeout > 20)
                {
                    #if debug
                    LogWarning("Location Service initialization failed - timed out");
                    #endif

                    return false;
                }
            }
            while (Input.location.status == LocationServiceStatus.Initializing);
            
//            while (Input.location.status == LocationServiceStatus.Initializing)
//
//            {
//                // Check in every half-second for 10 seconds - 'time out' if it takes longer than that.
//                
//                await UniTask.Delay(millisecondsDelay: 500);
//
//                if (++timeout > 20)
//                {
//                    #if debug
//                    LogWarning("Location Service initialization failed - timed out");
//                    #endif
//                    
//                    return false;
//                }
//            }

            #if debug
            Log(Input.location.status == LocationServiceStatus.Failed ? "Location Service initialization failed - unable to determine device location" : $"Location Service initialization success!\n\n[{Input.location.lastData.latitude}]\n[{Input.location.lastData.longitude}]\n[{Input.location.lastData.altitude}]");
            #endif

            return Input.location.status == LocationServiceStatus.Running;
        }

        public static void Stop() => Input.location.Stop();
        
        public static void UpdateSessionGeohash()
        {
            #if UNITY_EDITOR
            _sessionGeohash = GeoHasher.LatLongToHash(latitude: -37.780120f,
                                                      longitude: 144.892915f);
            #else 
            var location = Input.location.lastData;

            _sessionGeohash = GeoHasher.LatLongToHash(latitude: location.latitude,
                                                      longitude: location.longitude);
            #endif
            
            #if debug
            Log($"Session Geohash updated [{SessionGeohash}]");
            #endif
        }
        
    }

}