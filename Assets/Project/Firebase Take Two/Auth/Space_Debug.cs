using System.Threading.Tasks;

using Firebase.Extensions;
using Firebase.Firestore;

using UnityEngine;

using static UnityEngine.Debug;

public class Space_Debug : MonoBehaviour
{

	public string name;

	public string description;

	public double latitude;

	public double longitude;
	
	
	
//	public DocumentReference docRef;
	
//	[SerializeField]
//	public Space space = new Space();

	public async void Start()
	{
//		docRef = FirebaseFirestore.DefaultInstance.Collection(path: "spaces").Document(path: "template");

//		await DebugFetchRoomCollections();
		
//		docRef.Collection("beacons").GetSnapshotAsync()

	}


	[ContextMenu("Create Space")]
	private async void Create() => await Spatial.Spaces.Create(name: name,
	                                                           description: description,
	                                                           latitude: latitude,
	                                                           longitude: longitude);


//	[ContextMenu("Test Nearby Search")]
//	public async void TrySearch()
//	{
//		var result = await Spatial.Spaces.GetSpacesFromLatLong(lat: latitude,
//		                                                       lon: longitude);
//
//		foreach (var r in result)
//		{
//			Log("YEW "+r.docRef.Id);
//		}
//	}

	//	[ContextMenu(itemName: "Debug - Get Room Template")]
//	public async Task DebugFetchRoomTemplate() => await docRef.GetSnapshotAsync().ContinueWithOnMainThread(continuation: task =>
//	                                                                                                                     {
//		                                                                                                                     var snapshot = task.Result;
//
//		                                                                                                                     if (snapshot.Exists)
//		                                                                                                                     {
//			                                                                                                                     Log(message: $"Fetch successful for [{snapshot.Id}]");
//
//			                                                                                                                     space = snapshot.ConvertTo<Space>();
//
//			                                                                                                                     foreach (var pair in snapshot.ToDictionary())
//			                                                                                                                     {
//				                                                                                                                     Log(message: $"{pair.Key}\t\t{pair.Value}");
//			                                                                                                                     }
//		                                                                                                                     }
//		                                                                                                                     else
//		                                                                                                                     {
//			                                                                                                                     Log(message: $"Document {snapshot.Id} does not exist!");
//		                                                                                                                     }
//	                                                                                                                     });


//	[ContextMenu(itemName: "Debug - Get Room Template")]
//	public async Task DebugFetchRoomCollections() => await docRef.Collection(path: "beacons").Document(path: "template").GetSnapshotAsync().ContinueWithOnMainThread(continuation: task =>
//	                                                                                                                                                                               {
//		                                                                                                                                                                               var snapshot = task.Result;
//
//		                                                                                                                                                                               if (snapshot.Exists)
//		                                                                                                                                                                               {
//			                                                                                                                                                                               Log(message: $"Fetch successful for [{snapshot.Id}]");
//
//			                                                                                                                                                                               space = snapshot.ConvertTo<Space>();
//
//			                                                                                                                                                                               foreach (var pair in snapshot.ToDictionary())
//			                                                                                                                                                                               {
//				                                                                                                                                                                               Log(message: $"{pair.Key}\t\t{pair.Value}");
//			                                                                                                                                                                               }
//		                                                                                                                                                                               }
//		                                                                                                                                                                               else
//		                                                                                                                                                                               {
//			                                                                                                                                                                               Log(message: $"Document {snapshot.Id} does not exist!");
//		                                                                                                                                                                               }
//	                                                                                                                                                                               });

}
