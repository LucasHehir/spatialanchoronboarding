//using UnityEngine;
//using UnityEngine.Events;
//
//using System.Collections;
//
//public class ScreenFader : MonoBehaviour {
//
//	public static ScreenFader i;
//
//	[Header("Camera Material")]
//	[Tooltip("Populate this with the FaderShader material.")]
//	public Material camMaterial = null;
//
//	[Header("Fade Colour")]
//	public Color fadeColor;
//
//	[Header("Options")]
//	[Tooltip("Should we run a fade on Start [Automatically in the opposite direction of the 'current state' enum]")]
//	public bool fadeOnStart;
//
//	[Tooltip("If the fader is set to run on Start, should that particular fade run events?")]
//	public bool useEventsInStartFade;
//
//	[Tooltip("If a new fade instruction is received while one is already running, should we ignore it or run it?")]
//	public bool allowNewFadesToInterrupt;
//
//	[Tooltip("When a manual fade ends, should we run events?")]
//	public bool runEventsAtTheEndOfManualControl;
//
//	//[Tooltip("When a fade has completed, should we run another fade in the opposite direction?")]
//	//public bool runReverseFadeOnCompletion;
//
//	[Tooltip("If we're manually controlling the fader, should manual control end when it reaches 0?")]
//	public bool turnOffManualControlWhenNormalReachesZero;
//
//	[Tooltip("If we're manually controlling the fader, should manual control end when it reaches 1?")]
//	public bool turnOffManualControlWhenNormalReachesOne;
//
//	[Header("Timing")]
//	[Range(0, 1)]
//	public float fadeNormal;
//	public float preFadeDelay;
//	public float fadeTimeInSeconds;
//
//	public bool useUnscaledTime;
//
//	[Header("Status")]
//	public FadeState currentState;
//	public bool midCoroutine;
//	public bool toBlack;
//	public bool doPostRender;
//
//	[Header("Events")]
//	public UnityEvent onFadeToClearBegan;
//	public UnityEvent onFadeToBlackBegan;
//
//	public UnityEvent onFadeToClearCompleted;
//	public UnityEvent onFadeToBlackCompleted;
//
//	public IEnumerator currentFade;
//
//	void Awake() {
//		if (!i) {
//			i = this;
//		} else {
//			Error("The singleton already exists, ScreenFader must have been instantiated twice.");
//		}
//	}
//
//	void Start() {
//		if (onFadeToClearBegan == null) {
//			onFadeToClearBegan = new UnityEvent();
//		}
//
//		if (onFadeToBlackBegan == null) {
//			onFadeToBlackBegan = new UnityEvent();
//		}
//
//		if (onFadeToClearCompleted == null) {
//			onFadeToClearCompleted = new UnityEvent();
//		}
//
//		if (onFadeToBlackCompleted == null) {
//			onFadeToBlackCompleted = new UnityEvent();
//		}
//
//		if (currentState == FadeState.Faded) {
//			toBlack = true;
//			fadeNormal = 1.0f;
//			fadeColor.a = 1.0f;
//			camMaterial.color = fadeColor;
//			doPostRender = true;
//		}
//
//		if (fadeOnStart) {
//			if (useEventsInStartFade) {
//				HandlePingPongFade(true, false);
//			} else {
//				HandlePingPongFade(false, false);
//			}
//		}
//	}
//
//	// These parameters aren't the most clear, sorry!
//	// doEvents simply says whether to invoke events at the beginning and end.
//	// forceNewState and forceToBlack are simply for if you want to manually set which state to fade towards (i.e. true/true would force the transition to be 'black', true/false would force it to 'clear')
//	public void HandlePingPongFade(bool doEvents, bool forceNewState, bool forceToBlack = true) {
//		if (midCoroutine) {
//			if (allowNewFadesToInterrupt) {
//				Warning("Fade interrupted!");
//				StopCoroutine(currentFade);
//			} else {
//				Error("A fade instruction was denied because we're already mid-transition at the moment and interrupts haven't been switched on!");
//				return;
//			}
//		}
//
//		if (forceNewState) {
//			toBlack = forceToBlack;
//		} else {
//			toBlack = !toBlack;
//		}
//
//		currentFade = PingPongFade(doEvents);
//
//		StartCoroutine(currentFade);
//	}
//
//	public void BeginManualControl(bool turnOffManualModeAt0, bool turnOffManualModeAt1, bool runEventsAtTheEnd) {
//		if (currentState == FadeState.ManualControl) {
//			Error("Manual control mode is already active, no need to start it again.");
//			return;
//		}
//
//		if (midCoroutine && !allowNewFadesToInterrupt) {
//			Error("You can't manually control the fader right now because interrupts are currently disabled. If you want more control, you may need to add another seperate bool to allow this.");
//			return;
//		} else {
//			StopCoroutine(currentFade);
//		}
//
//		Log("Beginning manual fader control.");
//
//		turnOffManualControlWhenNormalReachesZero = turnOffManualModeAt0;
//		turnOffManualControlWhenNormalReachesOne = turnOffManualModeAt1;
//		runEventsAtTheEndOfManualControl = runEventsAtTheEnd;
//
//		currentState = FadeState.ManualControl;
//	}
//
//	public void SetFader(float value) {
//		if (currentState != FadeState.ManualControl) {
//			return;
//		}
//
//		fadeNormal = Mathf.Clamp01(value);
//
//		doPostRender = fadeNormal != 0.0f;
//
//		Color c = camMaterial.color;
//		c.a = Mathf.Lerp(0.0f, 1.0f, fadeNormal);
//		camMaterial.color = c;
//
//		//if (turnOffManualControlWhenNormalReachesOne) {
//		//	Error("This shouldnt happen");
//		//	if (fadeNormal == 1.0f) {
//		//		HandlePingPongFade(runEventsAtTheEndOfManualControl, true, true);
//		//	} else if (fadeNormal == 0.0f) {
//		//		HandlePingPongFade(runEventsAtTheEndOfManualControl, true, false);
//		//	}
//		//}
//	}
//
//	//public void EndManualControl() {
//	//	if (currentState != FadeState.ManualControl) {
//	//		Error("Manual control mode is already inactive, no need to turn it off again.");
//	//		return;
//	//	}
//
//	//	Log("Ending manual fader control.");
//
//	//	if (fadeNormal > 0.5f) {
//	//		Log("Because the fader normal is above 0.5f (you should see mostly colour) I'm going to run a fade-to-black from here.");
//	//		HandlePingPongFade(runEventsAtTheEndOfManualControl, true, true); // If the fade normal is already closer to being black, start a fade out from this point.
//	//	} else {
//	//		Log("Because the fader normal is below 0.5f (you shouldn't see anything) I'm going to run a fade-to-clear from here.");
//	//		HandlePingPongFade(runEventsAtTheEndOfManualControl, true, false); // If the fade normal is already closer to being clear, start a fade in from this point.
//	//	}
//
//	//	//HandlePingPongFade(false, true, false);
//	//}
//
//	IEnumerator PingPongFade(bool doEvents) {
//		Log("FadeNormal at start of fade is [" + fadeNormal + "]");
//
//		Log("Starting PingPongFade ToBlack? [" + toBlack+"]");
//
//		midCoroutine = true;
//		doPostRender = true;
//
//		// --- Invoke Begin events
//		if (doEvents) {
//			if (toBlack) {
//				onFadeToBlackBegan.Invoke();
//			} else {
//				onFadeToClearBegan.Invoke();
//			}
//		}
//
//		// --- Delay tickdown if any
//		if (preFadeDelay > 0) {
//			float delayTimer = 0;
//
//			while (delayTimer < preFadeDelay) {
//				delayTimer += (useUnscaledTime ? Time.unscaledDeltaTime : Time.deltaTime);
//				Log("preFadeDelay ticking down... ["+delayTimer+"]");
//				yield return null;
//			}
//		}
//
//		// --- Fading
//
//		float rate = 1.0f / fadeTimeInSeconds;
//
//		Color c = camMaterial.color = fadeColor;
//
//		if (toBlack) {
//			while (fadeNormal < 1.0f) {
//				fadeNormal += (useUnscaledTime ? Time.unscaledDeltaTime : Time.deltaTime) * rate;
//				c.a = Mathf.Lerp(0.0f, 1.0f, fadeNormal);
//				camMaterial.color = c;
//				//Log("Fading - [" + fadeNormal + "]");
//				yield return null;
//			}
//
//			currentState = FadeState.Faded;
//		} else {
//			while (fadeNormal > 0.0f) {
//				fadeNormal -= (useUnscaledTime ? Time.unscaledDeltaTime : Time.deltaTime) * rate;
//				c.a = Mathf.Lerp(0.0f, 1.0f, fadeNormal);
//				camMaterial.color = c;
//				//Log("Fading - [" + fadeNormal + "]");
//				yield return null;
//			}
//
//			currentState = FadeState.Clear;
//			doPostRender = false;
//		}
//
//		fadeNormal = Mathf.Clamp01(fadeNormal);
//
//		midCoroutine = false;
//
//		if (toBlack) {
//			Log("Fade to black complete.");
//		} else {
//			Log("Fade to clear complete.");
//		}
//
//		// --- Invoke Complete events
//
//		if (doEvents) {
//			if (toBlack) {
//				onFadeToBlackCompleted.Invoke();
//			} else {
//				onFadeToClearCompleted.Invoke();
//			}
//		}
//
//		//if (runReverseFadeOnCompletion) {
//		//	runReverseFadeOnCompletion = false;
//		//	HandlePingPongFade(doEvents, false);
//		//}
//	}
//
//	void OnPostRender() {
//		//if (doPostRender) {
//			camMaterial.SetPass(0);
//			GL.PushMatrix();
//			GL.LoadOrtho();
//			GL.Color(camMaterial.color);
//			GL.Begin(GL.QUADS);
//			GL.Vertex3(0f, 0f, -12f);
//			GL.Vertex3(0f, 1f, -12f);
//			GL.Vertex3(1f, 1f, -12f);
//			GL.Vertex3(1f, 0f, -12f);
//			GL.End();
//			GL.PopMatrix();
//		//}
//	}
//}