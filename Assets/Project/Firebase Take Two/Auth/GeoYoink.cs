using System;
using System.Collections.Generic;
using System.Linq;

using static UnityEngine.Debug;

namespace Vanilla.GeoHasher
{

    [Serializable]
    public static class GeoHasher
    {

        private const string c_Base32 = "0123456789bcdefghjkmnpqrstuvwxyz";

        private const int c_Base32_Char_BitSize = 5;

        private const double c_Min_Latitude = -90;
        private const double c_Max_Latitude = 90;

        private const double c_Min_Longitude = -180;
        private const double c_Max_Longitude = 180;

        private const int c_Default_Precision = 10;
        private const int c_Min_Precision     = 1;
        private const int c_Max_Precision     = 22;

        private static Dictionary<char, string[]> _neighbourLookup = null;
        private static Dictionary<char, string[]> NeighbourLookup =>
            _neighbourLookup ??= new Dictionary<char, string[]>
                                 {
                                     { 'n', new[] { "p0r21436x8zb9dcf5h7kjnmqesgutwvy", "bc01fg45238967deuvhjyznpkmstqrwx" } },
                                     { 's', new[] { "14365h7k9dcfesgujnmqp0r2twvyx8zb", "238967debc01fg45kmstqrwxuvhjyznp" } },
                                     { 'e', new[] { "bc01fg45238967deuvhjyznpkmstqrwx", "p0r21436x8zb9dcf5h7kjnmqesgutwvy" } },
                                     { 'w', new[] { "238967debc01fg45kmstqrwxuvhjyznp", "14365h7k9dcfesgujnmqp0r2twvyx8zb" } }
                                 };

        private static Dictionary<char, string[]> _borderLookup = null;
        private static Dictionary<char, string[]> BorderLookup =>
            _borderLookup ??= new Dictionary<char, string[]>
                              {
                                  { 'n', new[] { "prxz", "bcfguvyz" } },
                                  { 's', new[] { "028b", "0145hjnp" } },
                                  { 'e', new[] { "bcfguvyz", "prxz" } },
                                  { 'w', new[] { "0145hjnp", "028b" } }
                              };


        private static bool InvalidLatLong(double latitude,
                                           double longitude) => latitude  < c_Min_Latitude  ||
                                                                latitude  > c_Max_Latitude  ||
                                                                longitude < c_Min_Longitude ||
                                                                longitude > c_Max_Longitude;



        public static string LatLongToHash(double latitude,
                                           double longitude,
                                           int precision = c_Default_Precision)
        {
            var latMin = c_Min_Latitude;
            var latMax = c_Max_Latitude;

            var longMin = c_Min_Longitude;
            var longMax = c_Max_Longitude;

            var buffer = new char[precision];

            for (var i = 0;
                 i < precision;
                 i++)
            {
                var charIndex = 0;

                for (var j = 0;
                     j < c_Base32_Char_BitSize;
                     j++)
                {
                    if ((i * c_Base32_Char_BitSize + j) % 2 != 0)
                    {
                        // Latitude

                        var mid = (latMin + latMax) / 2;

                        if (latitude > mid)
                        {
                            charIndex = (charIndex << 1) + 1;

                            latMin = mid;
                        }
                        else
                        {
                            charIndex <<= 1;

                            latMax = mid;
                        }
                    }
                    else
                    {
                        // Longitude

                        var mid = (longMin + longMax) / 2;

                        if (longitude > mid)
                        {
                            charIndex = (charIndex << 1) + 1;

                            longMin = mid;
                        }
                        else
                        {
                            charIndex <<= 1;

                            longMax = mid;
                        }
                    }
                }

                buffer[i] = c_Base32[index: charIndex];
            }

            return new string(value: buffer);
        }

        public static(double, double) HashToLatLong(string input)
        {
            var (southWestLat, southWestLong, northEastLat, northEastLong) = GetGeohashBounds(geohash: input);

            return ((southWestLat + northEastLat) * 0.5f, (southWestLong + northEastLong) * 0.5f);
        }


//        public static bool isValidBase32string(string input)
//        {
//            var rgx = new Regex("^[" + BASE32_CHARS + "]*$");
//
//            return rgx.Matches(input: input).Count > 0;
//        }



//        public enum GeoQueryProximity
//        {
//
//            Accurate = 0,
//            CloseBy = 1,
//            Broad = 2
//
//        }



    /// <summary>
        ///     This is a convenience method that automatically handles sensible brackets for proximity.
        ///
        ///     The context for this app is AR + cloud anchors, so it is very unlikely we will need to know
        ///     about results further than a kilometre away.
        ///
        ///     You can bypass this method entirely and pass any geohash into GetNeighbours still.
        /// </summary>
        /// <param name="geohash">The hash... use GeoHasher.</param>
        /// <param name="proximity">
        ///     Accurate =  X 38m       Y 19m cells
        ///     CloseBy =   X 153m      Y 153m cells
        ///     Broad =     X 1.2km     Y 0.61km
        /// </param>
        /// <returns></returns>
//        public static string[] GetNeighbours(string geohash,
//                                             GeoQueryProximity proximity) => proximity switch
//                                                                             {
//                                                                                 GeoQueryProximity.Accurate => GetNeighboursUnrolled(geohash: geohash.Substring(startIndex: 0,
//                                                                                                                                                                length: 8)),
//                                                                                 GeoQueryProximity.CloseBy => GetNeighboursUnrolled(geohash: geohash.Substring(startIndex: 0,
//                                                                                                                                                               length: 7)),
//                                                                                 GeoQueryProximity.Broad => GetNeighboursUnrolled(geohash: geohash.Substring(startIndex: 0,
//                                                                                                                                                             length: 6))
//                                                                             };

          
//        private const string c_Even_North_Border = "prxz";
//        private const string c_Even_South_Border = "028b";
//        private const string c_Even_East_Border = "bcfguvyz";
//        private const string c_Even_West_Border = "0145hjnp";
//
//        private const string c_Even_North_Neighbour = "p0r21436x8zb9dcf5h7kjnmqesgutwvy";
//        private const string c_Even_South_Neighbour = "14365h7k9dcfesgujnmqp0r2twvyx8zb";
//        private const string c_Even_East_Neighbour  = "bc01fg45238967deuvhjyznpkmstqrwx";
//        private const string c_Even_West_Neighbour  = "238967debc01fg45kmstqrwxuvhjyznp";
//        
//        private const string c_Odd_North_Border = "bcfguvyz";
//        private const string c_Odd_South_Border = "0145hjnp";
//        private const string c_Odd_East_Border  = "prxz";
//        private const string c_Odd_West_Border  = "028b";
//
//        private const string c_Odd_North_Neighbour = "bc01fg45238967deuvhjyznpkmstqrwx";
//        private const string c_Odd_South_Neighbour = "238967debc01fg45kmstqrwxuvhjyznp";
//        private const string c_Odd_East_Neighbour  = "p0r21436x8zb9dcf5h7kjnmqesgutwvy";
//        private const string c_Odd_West_Neighbour  = "14365h7k9dcfesgujnmqp0r2twvyx8zb";

        // This version completes the cardinal directions first to avoid repeating work

        // This is now 40-50% faster

        // The last few directional GetNeighbour calls don't benefit from being removed since
        // they're used in a recursive fashion; nothing about them can be removed or re-used.

        public static string[] GetNeighboursUnrolled(string geohash)
        {
            
            const string c_Neighbour_A = "p0r21436x8zb9dcf5h7kjnmqesgutwvy";
            const string c_Neighbour_B = "14365h7k9dcfesgujnmqp0r2twvyx8zb";
            const string c_Neighbour_C = "bc01fg45238967deuvhjyznpkmstqrwx";
            const string c_Neighbour_D = "238967debc01fg45kmstqrwxuvhjyznp";

            const string c_Border_A = "prxz";
            const string c_Border_B = "028b";
            const string c_Border_C = "bcfguvyz";
            const string c_Border_D = "0145hjnp";
            
            var hashLength = geohash.Length;

            var lastCh = geohash[hashLength - 1];

            var parent = geohash.Substring(startIndex: 0,
                                           length: hashLength - 1);

            var results = new string[9];

            results[4] = geohash; // Set the center entry since we already have it
            
            string n; // cache the north result for later use with north-west and north-east

            string s; // cache the south result for later use with south-west and south-east
            
            // Per-direction

            if (hashLength % 2 == 0)
            {
                // Even

                if (parent.Length == 0)
                {
                    // North

                    n = results[1] = c_Base32[c_Neighbour_A.IndexOf(lastCh)].ToString();
                
                    // West
                
                    results[3] = c_Base32[c_Neighbour_D.IndexOf(lastCh)].ToString();
                
                    // East

                    results[5] = c_Base32[c_Neighbour_C.IndexOf(lastCh)].ToString();
                
                    // South

                    s = results[7] = c_Base32[c_Neighbour_B.IndexOf(lastCh)].ToString();
                }
                else
                {
                    // North

                    n = results[1] = c_Border_A.IndexOf(lastCh) != -1 ?
                                      GetNeighbour(geohash: parent,
                                                   direction: 'n') + c_Base32[c_Neighbour_A.IndexOf(lastCh)] :
                                      parent + c_Base32[c_Neighbour_A.IndexOf(lastCh)];
                
                    // West

                    results[3] = c_Border_D.IndexOf(lastCh) != -1 ?
                                     GetNeighbour(geohash: parent,
                                                  direction: 'w') + c_Base32[c_Neighbour_D.IndexOf(lastCh)] :
                                     parent + c_Base32[c_Neighbour_D.IndexOf(lastCh)];
                
                    // East

                    results[5] = c_Border_C.IndexOf(lastCh) != -1 ?
                                     GetNeighbour(geohash: parent,
                                                  direction: 'e') + c_Base32[c_Neighbour_C.IndexOf(lastCh)] :
                                     parent + c_Base32[c_Neighbour_C.IndexOf(lastCh)];
                
                    // South

                    s = results[7] = c_Border_B.IndexOf(lastCh) != -1 ?
                                      GetNeighbour(geohash: parent,
                                                   direction: 's') + c_Base32[c_Neighbour_B.IndexOf(lastCh)] :
                                      parent + c_Base32[c_Neighbour_B.IndexOf(lastCh)];
                }
                
            }
            else
            {
                // Odd
                
                if (parent.Length == 0)
                {
                    // North

                    n = results[1] = c_Base32[c_Neighbour_C.IndexOf(lastCh)].ToString();
                
                    // West
                
                    results[3] = c_Base32[c_Neighbour_B.IndexOf(lastCh)].ToString();
                
                    // East

                    results[5] = c_Base32[c_Neighbour_A.IndexOf(lastCh)].ToString();
                
                    // South

                    s = results[7] = c_Base32[c_Neighbour_D.IndexOf(lastCh)].ToString();
                }
                else
                {
                    // North

                    n = results[1] = c_Border_C.IndexOf(lastCh) != -1 ?
                                      GetNeighbour(geohash: parent,
                                                   direction: 'n') + c_Base32[c_Neighbour_C.IndexOf(lastCh)] :
                                      parent + c_Base32[c_Neighbour_C.IndexOf(lastCh)];
                
                    // West

                    results[3] = c_Border_B.IndexOf(lastCh) != -1 ?
                                     GetNeighbour(geohash: parent,
                                                  direction: 'w') + c_Base32[c_Neighbour_B.IndexOf(lastCh)] :
                                     parent + c_Base32[c_Neighbour_B.IndexOf(lastCh)];
                
                    // East

                    results[5] = c_Border_A.IndexOf(lastCh) != -1 ?
                                     GetNeighbour(geohash: parent,
                                                  direction: 'e') + c_Base32[c_Neighbour_A.IndexOf(lastCh)] :
                                     parent + c_Base32[c_Neighbour_A.IndexOf(lastCh)];
                
                    // South

                    s = results[7] = c_Border_D.IndexOf(lastCh) != -1 ?
                                     GetNeighbour(geohash: parent,
                                                  direction: 's') + c_Base32[c_Neighbour_D.IndexOf(lastCh)] :
                                     parent + c_Base32[c_Neighbour_D.IndexOf(lastCh)];
                }

            }
            
            // Now you can do NW/NE/SW/SE by referring to the already existing entries...

            // Here's the lazy directional calls for historys sake
            
//            results[0] = GetNeighbour(geohash: results[1],
//                                      direction: 'w');
//                
//            results[2] = GetNeighbour(geohash: results[1],
//                                      direction: 'e');
//
//            results[6] = GetNeighbour(geohash: results[7],
//                                      direction: 'w');
//
//            results[8] = GetNeighbour(geohash: results[7],
//                                      direction: 'e');
            
            // North...

            var n_HashLength = n.Length;

            var n_LastCh = n[hashLength - 1];

            var n_Parent = n.Substring(startIndex: 0,
                                      length: n_HashLength - 1);
            
            var n_EvenHashLength = n_HashLength % 2 == 0;

            if (n_EvenHashLength)
            {
                if (n_Parent.Length == 0)
                {
                    // ...West
                
                    results[0] = c_Base32[c_Neighbour_D.IndexOf(n_LastCh)].ToString();
                
                    // ...East
                
                    results[2] = c_Base32[c_Neighbour_C.IndexOf(n_LastCh)].ToString();
                }
                else
                {
                    // ...West
                
                    results[0] = c_Border_D.IndexOf(n_LastCh) != -1 ?
                                     GetNeighbour(geohash: n_Parent,
                                                  direction: 'w') + c_Base32[c_Neighbour_D.IndexOf(n_LastCh)] :
                                     n_Parent + c_Base32[c_Neighbour_D.IndexOf(n_LastCh)];
                
                    // ...East
                
                    results[2] = c_Border_C.IndexOf(n_LastCh) != -1 ?
                                     GetNeighbour(geohash: n_Parent,
                                                  direction: 'e') + c_Base32[c_Neighbour_C.IndexOf(n_LastCh)] :
                                     n_Parent + c_Base32[c_Neighbour_C.IndexOf(n_LastCh)];
                }
            }
            else
            {
                if (n_Parent.Length == 0)
                {
                    // ...West
                
                    results[0] = c_Base32[c_Neighbour_B.IndexOf(n_LastCh)].ToString();
                
                    // ...East
                
                    results[2] = c_Base32[c_Neighbour_A.IndexOf(n_LastCh)].ToString();
                }
                else
                {
                    // ...West
                
                    results[0] = c_Border_B.IndexOf(n_LastCh) != -1 ?
                                     GetNeighbour(geohash: n_Parent,
                                                  direction: 'w') + c_Base32[c_Neighbour_B.IndexOf(n_LastCh)] :
                                     n_Parent + c_Base32[c_Neighbour_B.IndexOf(n_LastCh)];
                
                    // ...East
                
                    results[2] = c_Border_A.IndexOf(n_LastCh) != -1 ?
                                     GetNeighbour(geohash: n_Parent,
                                                  direction: 'e') + c_Base32[c_Neighbour_A.IndexOf(n_LastCh)] :
                                     n_Parent + c_Base32[c_Neighbour_A.IndexOf(n_LastCh)];
                }
            }


            // South...
            
            var s_HashLength = s.Length;

            var s_LastCh = s[hashLength - 1];

            var s_Parent = s.Substring(startIndex: 0,
                                       length: s_HashLength - 1);
            
            var s_EvenHashLength = s_HashLength % 2 == 0;

            if (s_EvenHashLength)
            {
                if (s_Parent.Length == 0)
                {
                    // ...West
                
                    results[6] = c_Base32[c_Neighbour_D.IndexOf(s_LastCh)].ToString();
                
                    // ...East
                
                    results[8] = c_Base32[c_Neighbour_C.IndexOf(s_LastCh)].ToString();
                }
                else
                {
                    // ...West
                
                    results[6] = c_Border_D.IndexOf(s_LastCh) != -1 ?
                                     GetNeighbour(geohash: s_Parent,
                                                  direction: 'w') + c_Base32[c_Neighbour_D.IndexOf(s_LastCh)] :
                                     s_Parent + c_Base32[c_Neighbour_D.IndexOf(s_LastCh)];
                
                    // ...East
                
                    results[8] = c_Border_C.IndexOf(s_LastCh) != -1 ?
                                     GetNeighbour(geohash: s_Parent,
                                                  direction: 'e') + c_Base32[c_Neighbour_C.IndexOf(s_LastCh)] :
                                     s_Parent + c_Base32[c_Neighbour_C.IndexOf(s_LastCh)];
                }
            }
            else
            {
                if (s_Parent.Length == 0)
                {
                    // ...West
                
                    results[6] = c_Base32[c_Neighbour_B.IndexOf(s_LastCh)].ToString();
                
                    // ...East
                
                    results[8] = c_Base32[c_Neighbour_A.IndexOf(s_LastCh)].ToString();
                }
                else
                {
                    // ...West
                
                    results[6] = c_Border_B.IndexOf(s_LastCh) != -1 ?
                                     GetNeighbour(geohash: s_Parent,
                                                  direction: 'w') + c_Base32[c_Neighbour_B.IndexOf(s_LastCh)] :
                                     s_Parent + c_Base32[c_Neighbour_B.IndexOf(s_LastCh)];
                
                    // ...East
                
                    results[8] = c_Border_A.IndexOf(s_LastCh) != -1 ?
                                     GetNeighbour(geohash: s_Parent,
                                                  direction: 'e') + c_Base32[c_Neighbour_A.IndexOf(s_LastCh)] :
                                     s_Parent + c_Base32[c_Neighbour_A.IndexOf(s_LastCh)];
                }
            }
            
            return results;
        }
        
        /// <summary>
        ///     Returns a flattened array containing all neighbouring cells to the input, reading left to right, top to bottom starting with NW.
        /// </summary>
        /// <param name="geohash"></param>
        /// <returns></returns>
        public static string[] GetNeighboursArray(string geohash) => new[]
                                                                {
                                                                    GetNeighbour(geohash: GetNeighbour(geohash: geohash, direction: 'n'), direction: 'w'),
                                                                    GetNeighbour(geohash: geohash,                                        direction: 'n'),
                                                                    GetNeighbour(geohash: GetNeighbour(geohash: geohash, direction: 'n'), direction: 'e'),
                                                                    GetNeighbour(geohash: geohash,                                        direction: 'w'),
                                                                    GetNeighbour(geohash: geohash,                                        direction: 'e'),
                                                                    GetNeighbour(geohash: GetNeighbour(geohash: geohash, direction: 's'), direction: 'w'),
                                                                    GetNeighbour(geohash: geohash,                                        direction: 's'),
                                                                    GetNeighbour(geohash: GetNeighbour(geohash: geohash, direction: 's'), direction: 'e')
                                                                };
        
        // This is the original method, converted from JS @ github.com/davetroy/geohash-js
        public static string GetNeighbour(string geohash,
                                          char direction)
        {

            var hashLength = geohash.Length;

            var lastCh = geohash[hashLength - 1];

            var parent = geohash.Substring(startIndex: 0,
                                           length: hashLength - 1);
            
            var evenHashLength = hashLength % 2;

            if (BorderLookup[direction][evenHashLength].IndexOf(lastCh) != -1 &&
                parent                                                  != string.Empty)
            {
                parent = GetNeighbour(geohash: parent,
                                      direction: direction);
            }

            return parent + c_Base32[NeighbourLookup[direction][evenHashLength].IndexOf(lastCh)];
        }

        /// <summary>
        ///     Returns SW/NE latitude/longitude bounds of specified geohash.
        /// </summary>
        static(double,double,double,double) GetGeohashBounds(string geohash)
        {
            var evenBit = true;
            
            var latMin  = c_Min_Latitude;
            var latMax  = c_Max_Latitude;
            var lonMin  = c_Min_Longitude;
            var lonMax  = c_Max_Longitude;

            foreach (var idx in geohash.Select(chr => c_Base32.IndexOf(chr)))
            {
                if (idx == -1)
                {
                    LogError("Invalid geohash");

                    return default;
                }

                for (var n = 4;
                     n >= 0;
                     n--)
                {
                    var bitN = idx >> n & 1;

                    if (evenBit)
                    {
                        // Longitude
                        
                        var lonMid = (lonMin + lonMax) / 2;

                        if (bitN == 1)
                        {
                            lonMin = lonMid;
                        }
                        else
                        {
                            lonMax = lonMid;
                        }
                    }
                    else
                    {
                        // Latitude
                        
                        var latMid = (latMin + latMax) / 2;

                        if (bitN == 1)
                        {
                            latMin = latMid;
                        }
                        else
                        {
                            latMax = latMid;
                        }
                    }

                    evenBit = !evenBit;
                }
            }

            return (latMin, lonMin, latMax, lonMax);
        }

    }

}