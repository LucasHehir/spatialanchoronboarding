#if DEBUG_SPATIAL && SCENE_WELCOME
#define debug
#endif

using System.Linq;
using System.Threading.Tasks;

using Cysharp.Threading.Tasks;

using Firebase.Auth;

using RegexExamples;

using Spatial;

using UnityEngine;
using UnityEngine.SceneManagement;

using UnityEngine.UI;

using Vanilla.Initializer;
using Vanilla.ScreenFader;
using Vanilla.StringFormatting;

using static UnityEngine.Debug;

public class Scene_Welcome : MonoBehaviour, IInitiable
{

	[Header(header: "General")]
	public Text errorText;

	public Button welcomeCreateButton;
	public Button welcomeSignInButton;
	
	[Header(header: "Create Account Fields")]
	public InputField createUserNameField;
	public InputField createEmailField;
	public InputField createPasswordField;

	public Toggle createRememberMeToggle;
	
	public Button createBackButton;
	public Button createAccountButton;

	[Header(header: "Sign In")]
	public InputField signInEmailField;
	public InputField signInPasswordField;

	public Toggle signInRememberMeToggle;
	
	public Button signInBackButton;
	public Button signInButton;
	
	// Constants
	
	private const string c_Error_UserNameLength = "Please make sure your username is between 5-20 characters and only uses alphanumerics";

	private const string c_Error_EmailInvalid = "Please enter a real email address";
	private const string c_Error_PasswordLength = "Make sure your password is between 8-24 characters";

	// Character validation

	private static readonly char[] c_AllowedUserNameChars =
	{
//		'_',
//		'-',
//		'@',
//		'#',
//		'$',
//		'&',
	};

	private static readonly char[] c_AllowedEmailChars =
	{
		'_',
		'-',
		'@',
		'+',
		'.'
	};

	private static readonly char[] c_AllowedPasswordChars =
	{
		'_',
		'-',
		'@',
		'#',
		'$',
		'%',
		'&',
		'^',
		'*',
		'-',
		'+'
	};


	public UniTask Initialize()
	{
		ScreenFader.SetToDefault();

		Initializer.SceneInitializationComplete += InitializeComplete;
		
		return default;
	}


	public UniTask DeInitialize() => default;


	private async void InitializeComplete(Scene scene)
	{
		Initializer.SceneInitializationComplete -= InitializeComplete;

		ScreenFader.FadeIn();

		while (ScreenFader.FadeInProgress)
		{
			await Task.Yield();
		}

		// Are we already signed in from a previous session?

		if (FirebaseAuth.DefaultInstance.CurrentUser != null)
		{
			errorText.text = "Welcome back!\nSigning in...";

			// This next bit is weird.
			// If not on the Editor, run Location.Start and error-out/fallback to ActivateWelcomeUI if it fails.
			// If on Editor, bypass the Location.Start as well as the check for success.
			
#if !UNITY_EDITOR
			if (await Location.Start()) 
			{
#endif
				Location.UpdateSessionGeohash();

				ScreenFader.FadeOut();

				while (ScreenFader.FadeInProgress)
				{
					await Task.Yield();
				}

				NavigateToEarthScene();

				return;
#if !UNITY_EDITOR
			} 
			else 
			{
				errorText.text = "Automatic sign-in failed\nError starting location service";
			}
#endif
		}

		ActivateWelcomeUI();
	}


	private void ActivateWelcomeUI()
	{
		SetWelcomeUIInteractable(true);

		// Initialize Create Account fields

		createUserNameField.onValidateInput += (input,
		                                        charIndex,
		                                        addedChar) => ValidateUserNameChar(input: addedChar);

		createEmailField.onValidateInput += (input,
		                                     charIndex,
		                                     addedChar) => ValidateEmailChar(input: addedChar);

		createPasswordField.onValidateInput += (input,
		                                        charIndex,
		                                        addedChar) => ValidatePasswordChar(input: addedChar);

		// Initialize Sign-In fields

		var h = SystemInfo.deviceUniqueIdentifier.GetHashCode();

		if (PlayerPrefs.HasKey(key: FirebaseAuth_Utility.c_SignInCache_Email_Key))
		{
			#if debug
			Log($"encrypted email [{PlayerPrefs.GetString(FirebaseAuth_Utility.c_SignInCache_Email_Key)}]");
			Log($"decrypted email [{PlayerPrefs.GetString(FirebaseAuth_Utility.c_SignInCache_Email_Key).Xor(h)}]");
			#endif

			signInEmailField.text = PlayerPrefs.GetString(key: FirebaseAuth_Utility.c_SignInCache_Email_Key).Xor(key: h);
		}

		if (PlayerPrefs.HasKey(key: FirebaseAuth_Utility.c_SignInCache_Password_Key))
		{

			#if debug
			Log($"encrypted password [{PlayerPrefs.GetString(FirebaseAuth_Utility.c_SignInCache_Password_Key)}]");
			Log($"decrypted password [{PlayerPrefs.GetString(FirebaseAuth_Utility.c_SignInCache_Password_Key).Xor(h)}]");
			#endif

			signInPasswordField.text = PlayerPrefs.GetString(key: FirebaseAuth_Utility.c_SignInCache_Password_Key).Xor(key: h);

			signInRememberMeToggle.isOn = true;
		}

		signInEmailField.onValidateInput += (input,
		                                     charIndex,
		                                     addedChar) => ValidateEmailChar(input: addedChar);

		signInPasswordField.onValidateInput += (input,
		                                        charIndex,
		                                        addedChar) => ValidatePasswordChar(input: addedChar);
	}


	public void NavigateToEarthScene()
	{
		SceneManager.LoadSceneAsync(sceneName: "Earth",
		                            mode: LoadSceneMode.Additive);
		
		SceneManager.UnloadSceneAsync(scene: gameObject.scene);

		SceneManager.UnloadSceneAsync(sceneName: "UI Camera");
	}

	public void NavigateToMainMenuScene()
	{
		#if debug
		Log(message: "You're logged in! Move to next scene");
		#endif
		
		SceneManager.LoadSceneAsync(sceneName: "MainMenu", 
		                            mode: LoadSceneMode.Additive);
		
		SceneManager.UnloadSceneAsync(scene: gameObject.scene);
	}

	private char ValidateUserNameChar(char input) => char.IsLetter(c: input) || char.IsNumber(c: input) || c_AllowedUserNameChars.Any(predicate: c => c.Equals(obj: input)) ? input : '\0';

	private char ValidateEmailChar(char input) => char.IsLetter(c: input) || char.IsNumber(c: input) || c_AllowedEmailChars.Any(predicate: c => c.Equals(obj: input)) ? input : '\0';

	private char ValidatePasswordChar(char input) => char.IsLetter(c: input) || char.IsNumber(c: input) || c_AllowedPasswordChars.Any(predicate: c => c.Equals(obj: input)) ? input : '\0';
	
	public bool ValidateCreateFields()
	{
		
		var nameCount = createUserNameField.text.Length;

		if (nameCount < 5 || nameCount > 20)
		{
			errorText.text = c_Error_UserNameLength;

			return false;
		}
		
		if (!RegexUtilities.IsValidEmail(email: createEmailField.text))
		{
			errorText.text = c_Error_EmailInvalid;

			return false;
		}

		var passCount = createPasswordField.text.Length;
		
		if (passCount < 8 || passCount > 24)
		{
			errorText.text = c_Error_PasswordLength;

			return false;
		}

		errorText.text = string.Empty;

		return true;
	}
	
	public bool ValidateSignInFields()
	{
		if (!RegexUtilities.IsValidEmail(email: signInEmailField.text))
		{
			errorText.text = c_Error_EmailInvalid;

			return false;
		}

		var passCount = signInPasswordField.text.Length;
		
		if (passCount < 8 || passCount > 24)
		{
			errorText.text = c_Error_PasswordLength;

			return false;
		}

		errorText.text = string.Empty;

		return true;
	}


	[ContextMenu(itemName: "Delete Sign In Cache")]
	public void DeleteSignInCache()
	{
		if (PlayerPrefs.HasKey(key: FirebaseAuth_Utility.c_SignInCache_Username_Key))
		{
			PlayerPrefs.DeleteKey(key: FirebaseAuth_Utility.c_SignInCache_Username_Key);
		}
		
		if (PlayerPrefs.HasKey(key: FirebaseAuth_Utility.c_SignInCache_Email_Key))
		{
			PlayerPrefs.DeleteKey(key: FirebaseAuth_Utility.c_SignInCache_Email_Key);
		}
		
		if (PlayerPrefs.HasKey(key: FirebaseAuth_Utility.c_SignInCache_Password_Key))
		{
			PlayerPrefs.DeleteKey(key: FirebaseAuth_Utility.c_SignInCache_Password_Key);
		}
	}


	private void CacheSignInDetails(string userName = null,
	                                string email = null,
	                                string password = null)
	{
		
		var h = SystemInfo.deviceUniqueIdentifier.GetHashCode();

		if (userName != null)
		{
			PlayerPrefs.SetString(key: FirebaseAuth_Utility.c_SignInCache_Username_Key,
			                      value: userName.Xor(key: h));
		}

		if (email != null)
		{
			PlayerPrefs.SetString(key: FirebaseAuth_Utility.c_SignInCache_Email_Key,
			                      value: email.Xor(key: h));
		}

		if (password != null)
		{
			PlayerPrefs.SetString(key: FirebaseAuth_Utility.c_SignInCache_Password_Key,
			                      value: password.Xor(key: h));
		}
		
	}


	public async void CreateAccount()
	{

		if (!ValidateCreateFields()) return;

		errorText.text = string.Empty;

		SetCreateUIInteractable(false);

		var authSuccess = await FirebaseAuth_Utility.CreateAccount(email: createEmailField.text,
		                                                           password: createPasswordField.text);

		#if debug
		Log(message: $"Create account [{(authSuccess ? "success" : "failed")}]");
		#endif

		if (!authSuccess)
		{
			#if debug
			errorText.text = "Error creating account";
			#endif

			SetCreateUIInteractable(true);

			return;
		}
		
// Skip Location services for the Editor
#if !UNITY_EDITOR
		if (!await Location.Start())
		{
			#if debug
			errorText.text = "Error starting location service";
			#endif

			SetCreateUIInteractable(true);

			return;
		}
#endif

		if (createRememberMeToggle.isOn)
		{
			CacheSignInDetails(userName: createUserNameField.text,
			                   email: createEmailField.text,
			                   password: createPasswordField.text);
		}
		
		#if debug
		FirebaseAuth_Utility.LogCurrentUserDetails();
		#endif

		await Users.Create(userName: createUserNameField.text);
		
		Location.UpdateSessionGeohash();
		
		ScreenFader.FadeOut();

		while (ScreenFader.FadeInProgress)
		{
			await Task.Yield();
		}
		
		NavigateToEarthScene();

//		NavigateToMainMenuScene();

	}


	public async void SignIn()
	{
		if (!ValidateSignInFields()) return;

		SetSignInUIInteractable(false);

		var authSuccess = await FirebaseAuth_Utility.SignIn(email: signInEmailField.text,
		                                                    password: signInPasswordField.text);

		#if debug
		Log(message: $"Sign-in [{(authSuccess ? "success" : "failed")}]");
		#endif

		if (!authSuccess)
		{
			#if debug
			errorText.text = "Error signing in";
			#endif
			
			SetSignInUIInteractable(true);

			return;
		}

		// Skip Location services for the Editor
#if !UNITY_EDITOR
		if (!await Location.Start())
		{
			#if debug
			errorText.text = "Error starting location service";
			#endif

			SetSignInUIInteractable(true);

			return;
		}
#endif

		if (signInRememberMeToggle.isOn)
		{
			CacheSignInDetails(userName: null,
			                   email: signInEmailField.text,
			                   password: signInPasswordField.text);
		}

		await Users.SignIn();
		
		Location.UpdateSessionGeohash();

		ScreenFader.FadeOut();

		while (ScreenFader.FadeInProgress)
		{
			await Task.Yield();
		}
		
		NavigateToEarthScene();
		
//		NavigateToMainMenuScene();

	}


	private void SetWelcomeUIInteractable(bool state) => welcomeCreateButton.interactable = 
		                                                     welcomeSignInButton.interactable = 
			                                                     state;

	private void SetSignInUIInteractable(bool state) => signInEmailField.interactable =
		                                                   signInPasswordField.interactable =
			                                                   signInRememberMeToggle.interactable =
				                                                   signInBackButton.interactable =
					                                                   signInButton.interactable = state;


	private void SetCreateUIInteractable(bool state) => createUserNameField.interactable =
		                                                    createEmailField.interactable =
			                                                    createPasswordField.interactable =
				                                                    createRememberMeToggle.interactable =
					                                                    createBackButton.interactable =
						                                                    createAccountButton.interactable = state;


}