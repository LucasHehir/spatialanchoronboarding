using UnityEngine;
using UnityEngine.UI;

using Vanilla.ScreenFader;

public class ScreenFader_UI : ScreenFader
{

	public Image image;

	protected override void FadeFrame(Color a) => image.color = a;

	protected override void FadeInStart() => image.transform.root.gameObject.SetActive(true);

	protected override void FadeInEnd() => image.transform.root.gameObject.SetActive(false);

	protected override void FadeOutStart() => image.transform.root.gameObject.SetActive(true);

	protected override void FadeOutEnd() => image.transform.root.gameObject.SetActive(true);

	[ContextMenu("Debug Fade In")]
	private void DebugFadeIn() => FadeIn();

	[ContextMenu("Debug Fade Out")]
	private void DebugFadeOut() => FadeOut();
}