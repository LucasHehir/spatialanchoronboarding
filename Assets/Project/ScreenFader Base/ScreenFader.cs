#if DEBUG_VANILLA && SCREEN_FADER
#define debug
#endif

using System;
using System.Collections;

using Cysharp.Threading.Tasks;

using UnityEngine;

namespace Vanilla.ScreenFader
{
	
	public abstract class ScreenFader : MonoBehaviour, IInitiable
	{

		public static Color FadeColor = new Color(r: 0.1f,
		                                          g: 0.1f,
		                                          b: 0.1f,
		                                          a: 1.0f);
		
		private static bool _fadeInProgress = false;
		public static  bool FadeInProgress => _fadeInProgress;

		private static float _fadeProgress = 1.0f;
		public static  float FadeProgress => _fadeProgress;
		
		private static float _fadeTime = 1.0f;
		public static  float FadeTime => _fadeTime;

		private static IEnumerator _currentFade;
//		public static  IEnumerator CurrentFade => _currentFade;

		public static Action OnFadeInComplete;
		public static Action OnFadeOutComplete;

		public static ScreenFader fader;
		
		public UniTask Initialize()
		{
			#if debug
			Debug.Log("I'm initializin'!");
			#endif
			
			// Any change-over of fader (i.e. 2D UI fader to AR camera fader)
			// should be accompanied by a cancellation of the current/previous fader.
			
			Cancel();

			fader = this;

			return default;
		}

		public UniTask DeInitialize()
		{
			#if debug
			Debug.Log("I'm de-initializin'!");
			#endif
			
			// Any change-over of fader (i.e. 2D UI fader to AR camera fader)
			// should be accompanied by a cancellation of the current/previous fader.
			
			Cancel();

			fader = null;

			return default;
		}


		public static void SetToDefault()
		{
			_fadeProgress = 1.0f;

			FadeColor.a = _fadeProgress;
			
			fader.FadeFrame(FadeColor);
		}
		
		public static void FadeIn()
		{
			Cancel();

			_currentFade = fader._FadeIn();

			fader.StartCoroutine(routine: _currentFade);
		}
		
		public static void FadeOut()
		{
			Cancel();

			_currentFade = fader._FadeOut();
			
			fader.StartCoroutine(routine: _currentFade);
		}


		public static void Cancel()
		{
			if (!ReferenceEquals(objA: _currentFade,
			                     objB: null)) fader.StopCoroutine(routine: _currentFade);
		}

		private IEnumerator _FadeIn()
		{
			FadeInStart();
			
			_fadeInProgress = true;
			
			var rate = 1.0f / _fadeTime;

			while (_fadeProgress > 0.0f)
			{
				_fadeProgress = Mathf.Clamp01(_fadeProgress - (Time.deltaTime * rate));

				FadeColor.a = _fadeProgress;
				
				fader.FadeFrame(FadeColor);
				
				#if debug
				Debug.Log($"Fade frame - progress [{_fadeProgress}]");
				#endif

				yield return null;
			}
			
			_fadeInProgress = false;
			
			FadeInEnd();
			
			OnFadeInComplete?.Invoke();
		}


		private IEnumerator _FadeOut(float timeInSeconds = 1.5f)
		{
			FadeOutStart();
			
			_fadeInProgress = true;
			
			var rate = 1.0f / timeInSeconds;

			while (_fadeProgress < 1.0f)
			{
				_fadeProgress = Mathf.Clamp01(_fadeProgress + (Time.deltaTime * rate));

				FadeColor.a = _fadeProgress;
				
				fader.FadeFrame(FadeColor);
				
				#if debug
				Debug.Log($"Fade frame - progress [{_fadeProgress}]");
				#endif
				
				yield return null;
			}
			
			_fadeInProgress = false;
			
			FadeOutEnd();
			
			OnFadeOutComplete?.Invoke();
		}

		protected abstract void FadeFrame(Color c);

		protected abstract void FadeInStart();
		protected abstract void FadeInEnd();
		
		protected abstract void FadeOutStart();
		protected abstract void FadeOutEnd();

	}

}