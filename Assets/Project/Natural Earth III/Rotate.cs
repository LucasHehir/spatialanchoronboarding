using UnityEngine;

public class Rotate : MonoBehaviour
{

    public float rate = 1.0f;

    private Transform t;

    void Awake() => t = transform;


    void Update() => t.Rotate(xAngle: 0,
                              yAngle: rate * Time.deltaTime,
                              zAngle: 0);

}
