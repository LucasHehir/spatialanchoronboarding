using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

using Cysharp.Threading.Tasks;

using UnityEngine;

using Vanilla.Geodetics;

public class Earth_Ping : MonoBehaviour
{

	public Vector3 testGeodetic;

	private Transform t;

	public Transform      child;
	public SpriteRenderer sprite;

	public Transform relative;

	public Vector3 scaleMin = Vector3.one;

	public Vector3 scaleMax = Vector3.one;

	public float alphaMin = 1.0f;
	
	public float alphaMax = 0.0f;
	
	public float pingTimer = 1.0f;
	
	void Awake() => t = transform;

	[ContextMenu(itemName: "Test Ping")]
	public void TestPing()
	{
		Ping(geo: testGeodetic);
	}

	public async UniTask Ping(Vector3 geo)
	{
//		t.position = relative.TransformPoint(geo.GeodeticToCartesian());

		t.localPosition = geo.GeodeticToCartesian();

		t.forward = t.position - relative.position;

		var i    = 0.0f;
		var rate = 1.0f / pingTimer;

		var c = sprite.color;
		
		while (i < 1.0f)
		{
			child.localScale = Vector3.Lerp(a: scaleMin,
			                                b: scaleMax,
			                                t: i);

			c.a = Mathf.Lerp(a: alphaMin,
			                 b: alphaMax,
			                 t: i);

			sprite.color = c;
			
			i += rate * Time.deltaTime;

			await Task.Yield();
		}

		child.localScale = scaleMax;
		
		c.a = alphaMax;

		sprite.color = c;
		
	}
	
	

}