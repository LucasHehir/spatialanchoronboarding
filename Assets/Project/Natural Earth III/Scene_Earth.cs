#if DEBUG_SPATIAL && SCENE_EARTH
#define debug
#endif

using Cysharp.Threading.Tasks;

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements.Experimental;

using Vanilla.Geodetics;
using Vanilla.GeoHasher;
using Vanilla.Initializer;
using Vanilla.ScreenFader;

namespace Spatial
{

	public class Scene_Earth : MonoBehaviour, IInitiable
	{

		public Earth_Ping ping;

		[SerializeField]
		private GeodeticTransform _geoTransform;
		public GeodeticTransform GeoTransform => _geoTransform ? _geoTransform : _geoTransform = GetComponent<GeodeticTransform>();

		[SerializeField]
		public int hashLoopStart = 1;
		[SerializeField]
		public int hashLoopEnd   = 4;

		[SerializeField]
		public float startZoom = 200.0f;
		[SerializeField]
		public float endZoom   = 65.0f;

		private double x;
		private double y;
		
		public UniTask Initialize()
		{
			SceneManager.SetActiveScene(scene: gameObject.scene);

			// We want to start with a dramatic spin around + zoom out from the world starting at the polar opposite of the first ping.
			// In order to achieve this, we need to set the camera position so its accurately opposite the first ping position.

			// Firstly, we get that initial imprecise lat/long point.

			(y, x) = GeoHasher.HashToLatLong(input: Location.SessionGeohash.Substring(startIndex: 0,
			                                                                          length: hashLoopStart));
			
			// Turn that lat/long into a cartesian position, localized to the _geoTransforms target transform
			
			_geoTransform.transform.position = _geoTransform.target.TransformPoint(position: new Vector3(x: (float) x + 180.0f,
			                                                                                             y: (float) y + 90.0f,
			                                                                                             z: endZoom).GeodeticToCartesian());

			// If your geodetic transform uses smoothing, make sure to set it's delta to that same value as well (as a geodetic).
			// Otherwise direct position sets like the one above will be ignored/overridden!
			
			_geoTransform.delta = _geoTransform.transform.position.CartesianToGeodetic();
			
			// Set the geodetic transforms destination to the lat/long of the imprecise hash.
			
			_geoTransform.destination = new Vector3(x: (float) x,
			                                        y: (float) y,
			                                        z: startZoom);

			// With everything in place, we can begin the animation

			Initializer.SceneInitializationComplete += OnSceneInitComplete;
			
			return default;
		}
		
		public UniTask DeInitialize() => default;

		public void OnSceneInitComplete(Scene scene)
		{
			Initializer.SceneInitializationComplete -= OnSceneInitComplete;
			
			Animate();
		}

		private async UniTask Animate()
		{
			
			ScreenFader.FadeIn();
			
			_geoTransform.enabled = true;

			#if debug
			Debug.Log(message: $"Precision [{hashLoopStart}] - Hash [{Location.SessionGeohash.Substring(startIndex: 0, length: hashLoopStart)}] Long [{x}] Lat [{y}] Alt [{startZoom}]");
			#endif

			await UniTask.Delay(millisecondsDelay: 4000);
			
			// Technically the first ping point is done, so we start the loop an extra index in
			// We can follow similar steps to above now, except without worrying about setting the start position

			for (var i = hashLoopStart + 1; i <= hashLoopEnd; i++)
			{
				
				// Figure out the next ping site, with its z set to just above Earths surface
				
				(y, x) = GeoHasher.HashToLatLong(input: Location.SessionGeohash.Substring(startIndex: 0,
				                                                                          length: i));
				
				var d = new Vector3(x: (float) x,
				                    y: (float) y,
				                    z: endZoom);
				
				// Call a ping animation using this position,
				
				await ping.Ping(geo: d);

				// Figure out how zoomed in the camera should be for this next point
				
				var zLerp = (float) (i - hashLoopStart) / (hashLoopEnd - hashLoopStart);

				// Modify z so its 'eased out' - aka earlier values take bigger steps than later ones
				// We do this to de-exaggerate the zoom values as we get closer to the Earth
				// Otherwise it appears to get a LOT faster as we approach the surface
				
				var zEased = Easing.OutSine(t: zLerp);

				d.z = Mathf.Lerp(a: startZoom,
				                 b: endZoom,
				                 t: zEased);
				
				_geoTransform.destination = d;

				#if debug
				Debug.Log(message: $"Precision [{i}] - Hash [{Location.SessionGeohash.Substring(startIndex: 0, length: i)}] Destination [{d}] Z Lerp [{zLerp}]");
				#endif
				
				// Wait a bit for dramatic effect
				
				await UniTask.Delay(millisecondsDelay: 1500);

			}
			
			ScreenFader.FadeOut();

			while (ScreenFader.FadeInProgress)
			{
				await UniTask.Yield();
			}

//			NavigateToMainMenu();

			await SceneManager.LoadSceneAsync(sceneName: "UI Camera",
			                                  mode: LoadSceneMode.Additive);

			await SceneManager.LoadSceneAsync(sceneName: "MainMenu", 
			                                  mode: LoadSceneMode.Additive);

			SceneManager.UnloadSceneAsync(scene: gameObject.scene);
			
		}

//		private void NavigateToMainMenu()
//		{
//			SceneManager.LoadSceneAsync(sceneName: "UI Camera");
//
//			SceneManager.LoadSceneAsync(sceneName: "MainMenu");
//
//			SceneManager.UnloadSceneAsync(scene: gameObject.scene);
//		}


//		private async UniTask DebugAwait()
//		{
//			#if UNITY_EDITOR
//				while (!Input.GetKeyDown(key: key)) await UniTask.Yield();
//			#else
//				// Untested!
//				while (Input.touchCount == 0 || Input.GetTouch(0).phase != TouchPhase.Began) await UniTask.Yield();
//			#endif
//			
//			await UniTask.Yield();
//		}

	}

}