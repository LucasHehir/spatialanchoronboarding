//using System.Collections;
//using System.Collections.Generic;
//
//using Cysharp.Threading.Tasks;
//
//using UnityEngine.XR.ARFoundation;
//
//namespace Vanilla.MetaScript.ForARFoundation
//{
//
//	public class Await_AR_Session_Tracking_Stability : TaskBase
//	{
//
//		public override string GetDescription() => "Wait for AR Tracking stability";
//
//
//		public override async UniTask Run()
//		{
//			while (ARSession.state != ARSessionState.SessionTracking)
//			{
//				await UniTask.Yield();
//			}
//		}
//
//	}
//
//}