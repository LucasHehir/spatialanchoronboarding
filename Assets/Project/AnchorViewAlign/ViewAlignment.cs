using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using Vanilla.DotNetExtensions;
using Vanilla.FloatModifiers;
using Vanilla.Geodetics;
using Vanilla.UnityExtensions;

public class ViewAlignment : MonoBehaviour
{

	public static ViewAlignment i; // Only for DebugSlider stuff, delete later

	public Transform anchor;

	public Transform viewTrans;

	public Transform camTrans;

	public Transform guideTrans;

	public SpriteRenderer[] viewSprites;
	
	public Color nonAlignedColor = Color.white;
	public Color alignedColor;
	
	public Vector2 guideDir;
	
	public float guideToDegrees;

	public Vector3 position;

	public SinCustomNormalModule horizontal;

	public SinCustomNormalModule vertical;

	public float lookingAtAnchorLeniencyDegrees = 15.0f;

	public float lookingThruViewLeniencyDegrees = 15.0f;

	[SerializeField]
	public bool camIsLookingAtAnchor;

	[SerializeField]
	public bool camFwdSameAsViewFwd;

	[SerializeField]
	private bool _camViewAligned;
	public bool camViewAligned
	{
		get => _camViewAligned;
		set
		{
			if (_camViewAligned == value) return;

			_camViewAligned = value;

			ViewAlignmentChanged();
		}
	}

	[SerializeField]
	public float hRateMax;
	
	[SerializeField]
	public float vRateMax;

	[SerializeField]
	public float spinUp;

	[SerializeField]
	public float spinUpRate = 1.0f;

	[Header("Debug")]
	public Toggle camIsLookingAtAnchorToggle;
	public Toggle camFwdSameAsViewFwdToggle;
	public Toggle camViewAlignedToggle;
	
	
	private void Start()
	{
		i        = this;
		
		hRateMax = horizontal.rate;
		vRateMax = vertical.rate;

		horizontal.rate = 0.0f;
		vertical.rate   = 0.0f;

//		position.radius = camTrans.position.magnitude;
	}


	void Update()
	{
		var oldPosition = position;
		
		camIsLookingAtAnchor = camTrans.IsLookingAt(target: anchor.position,
		                                            marginOfErrorInDegrees: lookingAtAnchorLeniencyDegrees);

		camIsLookingAtAnchorToggle.isOn = camIsLookingAtAnchor;
		
		camFwdSameAsViewFwd = Vector3.Dot(lhs: camTrans.forward,
		                                  rhs: viewTrans.forward) > lookingThruViewLeniencyDegrees.DegreesToDotProduct();

		camFwdSameAsViewFwdToggle.isOn = camFwdSameAsViewFwd;

		camViewAligned = camIsLookingAtAnchor && camFwdSameAsViewFwd;

		camViewAlignedToggle.isOn = camViewAligned;
		
		spinUp = (spinUp + Time.deltaTime * (camViewAligned ? spinUpRate : -spinUpRate)).Clamp01();

		if (spinUp > 0.0f)
		{
			horizontal.rate = Mathf.Lerp(a: 0.0f,
			                             b: hRateMax,
			                             t: spinUp);

			vertical.rate = Mathf.Lerp(a: 0.0f,
			                           b: vRateMax,
			                           t: spinUp);

			position.y  = horizontal.Calculate().ToRadians();
			position.x = vertical.Calculate().ToRadians();

//			viewTrans.SetSphericalPositionLocal(position: position);

			viewTrans.position = position.GeodeticToCartesian();
			
			viewTrans.LookAt(worldPosition: anchor.position);

			if (camViewAligned)
			{

				var gE = guideTrans.localEulerAngles;

				guideDir = new Vector2(x: position.y  - oldPosition.y,
				                       y: position.x - oldPosition.x).normalized;

				guideToDegrees = -Mathf.Atan2(y: guideDir.x,
				                              x: guideDir.y) * Mathf.Rad2Deg;

				gE.z = guideToDegrees;

				guideTrans.localEulerAngles = gE;

			}

			var c = Color.Lerp(a: nonAlignedColor,
			                   b: alignedColor,
			                   t: spinUp);
			
			foreach (var s in viewSprites)
			{
				s.color = c;
			}
		}
	}


	private void ViewAlignmentChanged()
	{
		guideTrans.gameObject.SetActive(_camViewAligned);
	}
	
}