using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class ComponentToggler : MonoBehaviour
{

	public MonoBehaviour thing;

	public void Toggle() => thing.enabled = !thing.enabled;

}