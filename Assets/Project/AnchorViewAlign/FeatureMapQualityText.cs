using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

using Google.XR.ARCoreExtensions;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;


public class FeatureMapQualityText : MonoBehaviour
{

	public Transform camOffsetTransform;
	
	public Transform camTrans;

	public GameObject viewGameObject;
	public ViewAlignment viewAlignment;

	public Button enableButton;
	
	public ARAnchorManager anchorMan;

	public Text text;

	public FeatureMapQuality _quality = FeatureMapQuality.Insufficient;

	public FeatureMapQuality quality
	{
		get => _quality;
		set
		{
//			if (_quality == value) return;

			_quality = value;

			text.text = _cache + _quality;
		}
	}

	public float featureMapQualityChecksPerSecond = 4;

	public Gradient colorGradient;

	public Image qualityScoreBar;

	public float idealQualityAssessmentTime = 10.0f;

	[SerializeField]
	[Range(min: 0,
	       max: 1)]
	private float _qualityScore = 0.0f;

	public float qualityScore
	{
		get => _qualityScore;
		set => _qualityScore = Mathf.Clamp01(value);
	}

	public float insufficientMultiplier = -1.0f;
	public float sufficientMultiplier   = 1.0f;
	public float goodMultiplier         = 2.0f;

	public bool updating;

	public string _cache;

	void Awake() => _cache = text.text;


	async void Start()
	{
//		while (ARSession.state != ARSessionState.SessionTracking)
//		{
//			await Task.Yield();
//		}

//		updating = true;

//		InvokeRepeating(methodName: nameof(Check),
//		                time: 0.0f,
//		                repeatRate: 1.0f / featureMapQualityChecksPerSecond);
	}


	void OnEnable()
	{
		// The plan here is to get the camera spherical position relative to the anchor (might be a bit hard now that it's not at 0,0,0?)
		// turn the lat/long into a Vector2 delta, then scale mod by the magnitude.
		// That way we know they're definitely moving around, although it could just be back and forth on the spot (they could cheese it)
		
		// Also maybe consider doing manual alignment again, and simply passing in a corrected/translated pose to EstimateFeatureMapQualityForHosting
		
//		camPosS = camTrans.GetSphericalPosition();
		
		StartCoroutine(nameof(Scan));
	}


	public IEnumerator Scan()
	{
		viewGameObject.SetActive(true);
		viewAlignment.gameObject.SetActive(true);

		enableButton.interactable = false;

		qualityScore = 0.0f;

		InvokeRepeating(methodName: nameof(Check),
		                time: 0.0f,
		                repeatRate: 1.0f / featureMapQualityChecksPerSecond);
			
		qualityScoreBar.fillAmount = qualityScore;
		qualityScoreBar.color      = colorGradient.Evaluate(qualityScore);

		while (qualityScore < 1.0f)
		{
			var rate = 1.0f / idealQualityAssessmentTime;

			var mod = 0.0f;

			if (viewAlignment.camIsLookingAtAnchor)
			{
				mod = _quality switch
				      {
					      FeatureMapQuality.Insufficient => Time.deltaTime * (rate * insufficientMultiplier),
					      FeatureMapQuality.Sufficient   => Time.deltaTime * (rate * sufficientMultiplier),
					      FeatureMapQuality.Good         => Time.deltaTime * (rate * goodMultiplier),
					      _                              => 0.0f
				      };
			}
			
			

			qualityScore += mod;

			qualityScoreBar.fillAmount = qualityScore;
			qualityScoreBar.color      = colorGradient.Evaluate(qualityScore);

			yield return null;
		}

		viewGameObject.SetActive(false);
		viewAlignment.gameObject.SetActive(false);

		enabled = false;
		
		enableButton.interactable = true;

		CancelInvoke();
	}

//	void Update()
//	{
//		if (!updating) return;
//
//		var rate = 1.0f / idealQualityAssessmentTime;
//
//		var mod = _quality switch
//		          {
//			          FeatureMapQuality.Insufficient => Time.deltaTime * (rate * insufficientMultiplier),
//			          FeatureMapQuality.Sufficient   => Time.deltaTime * (rate * sufficientMultiplier),
//			          FeatureMapQuality.Good         => Time.deltaTime * (rate * goodMultiplier),
//			          _                              => 0.0f
//		          };
//
//		qualityScore += mod;
//
//		qualityScoreBar.fillAmount = qualityScore;
//		qualityScoreBar.color      = colorGradient.Evaluate(qualityScore);
//	}


//	public void Check() => quality = anchorMan.EstimateFeatureMapQualityForHosting(pose: new Pose(position: camTrans.position,
//	                                                                                              rotation: camTrans.rotation));

	public void Check() => quality = anchorMan.EstimateFeatureMapQualityForHosting(pose: new Pose(position: camTrans.position,
	                                                                                              rotation: camTrans.rotation));

}