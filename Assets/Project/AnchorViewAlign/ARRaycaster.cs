using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

//using Vanilla.DataAssets;

public class ARRaycaster : MonoBehaviour
{

	public ARRaycastManager rayMan;

//	public BoolSocket enabledSocket;

//	public Vector3Socket tapBeginSocket;
//	public Vector3Socket tapFrameSocket;

	private List<ARRaycastHit> results = new List<ARRaycastHit>();


	void Awake()
	{
//		enabledSocket.OnBroadcast += ToggleEnabled;
	}

	private void OnDestroy()
	{
//		enabledSocket.OnBroadcast -= ToggleEnabled;
	}

	void OnValidate() => rayMan = FindObjectOfType<ARRaycastManager>();

	public void ToggleEnabled (bool state) => enabled = state;

	public void Update()
	{
		if (Input.touchCount        < 1) return;

		var t = Input.GetTouch(0);

		var hit = rayMan.Raycast(screenPoint: t.position,
		                         hitResults: results,
		                         trackableTypes: TrackableType.PlaneEstimated);

		if (!hit) return;

		var p = results[0].pose.position;
		
		if (t.phase == TouchPhase.Began)
		{
//			tapBeginSocket.Set(p);
		}
		
//		tapFrameSocket.Set(p);

	}
}