public class VSpeedSlider : DebugSlider
{

	protected override void Apply(float lerp)
	{
		base.Apply(lerp);

		ViewAlignment.i.vRateMax = lerp;
	}

}