//using System.Collections;
//using System.Collections.Generic;
//
//using Cysharp.Threading.Tasks;
//
//using Vanilla.MetaScript;
//
//public class Await_Event_Invocation : TaskBase
//{
//
//	public EventSocket socket;
//
//	public override string GetDescription() => $"Await the invocation of [{(socket.AssetAssigned ? socket.asset.name : MetaScript.Unknown)}]";
//
//	public async override UniTask Run()
//	{
//		var proceed = false;
//
//		void Proceed() => proceed = true;
//
//		socket.asset.onBroadcast += Proceed;
//		
//		while (proceed)
//		{
//			await UniTask.Yield();
//		}
//		
//		socket.asset.onBroadcast -= Proceed;
//	}
//
//}