using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class DebugSlider : MonoBehaviour
{

	public Text text;

	public string textCache;
	
	public ViewAlignment view;

	public float min = 0.05f;
	public float max = 2.0f;

	public int roundingNum;

	void Awake()
	{
		text      = transform.parent.GetComponentInChildren<Text>();
		textCache = text.text;
			
		GetComponent<Slider>().onValueChanged.AddListener(Modify);
	}
	
	public void Modify(float input) => Apply(lerp: Mathf.Lerp(a: min,
	                                                          b: max,
	                                                          t: input));


	protected virtual void Apply(float lerp) => text.text = $"{textCache}: {Math.Round(value: lerp, digits: roundingNum)}";

}
