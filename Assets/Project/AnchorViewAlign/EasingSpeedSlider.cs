public class EasingSpeedSlider : DebugSlider
{

	protected override void Apply(float lerp)
	{
		base.Apply(lerp);
		
		ViewAlignment.i.spinUpRate = lerp;
	}

}