public class ViewLeniencySlider : DebugSlider
{

	protected override void Apply(float lerp)
	{
		base.Apply(lerp);

		ViewAlignment.i.lookingThruViewLeniencyDegrees = lerp;
	}

}