using System;

using UnityEngine;

namespace Vanilla.FloatModifiers
{

	[Serializable]
	public struct SinModule
	{

		[Header(header: "Settings")] [SerializeField]
		public float rate;

		[Range(min: 0.0f,
		       max: 1.0f)]
		[SerializeField]
		public float timeOffset;

		[Header(header: "Output")] 
		[SerializeField]
		public float time;

		[SerializeField]
		public float sin;

		private const float DoublePI = Mathf.PI * 2.0f;


		public float Calculate()
		{
			time += Time.deltaTime * rate;

			if (time > DoublePI)
			{
				time -= DoublePI;
			}

			return sin = Mathf.Sin(f: 0.5f + timeOffset * DoublePI + time);
		}


		public void Reset() => time = 0.0f;


	}

	[Serializable]
	public struct SinNormalModule
	{

		[Header("Settings")] [SerializeField]
		public float rate;

		[Range(min: 0.0f,
		       max: 1.0f)]
		[SerializeField]
		public float timeOffset;

		[Header("Output")] 
		[SerializeField]
		public float time;

		[SerializeField]
		public float sin;

		[SerializeField]
		public float normal;

		private const float DoublePI = Mathf.PI * 2.0f;


		public float Calculate()
		{
			time += Time.deltaTime * rate;

			if (time > DoublePI)
			{
				time -= DoublePI;
			}

			sin = Mathf.Sin(f: 0.5f + timeOffset * DoublePI + time);

			return normal = 0.5f + sin * 0.5f;
		}


		public void Reset() => time = 0.0f;

	}

	[Serializable]
	public struct SinCustomNormalModule
	{

		[Header(header: "Settings")] [SerializeField]
		public float rate;

		[Range(min: 0.0f,
		       max: 1.0f)]
		[SerializeField]
		public float timeOffset;

		[SerializeField]
		public float min;

		[SerializeField]
		public float max;

		[Header(header: "Output")] 
		[SerializeField]
		public float time;

		[SerializeField]
		public float sin;

		[SerializeField]
		private float normal;

		private const float DoublePI = Mathf.PI * 2.0f;


		public float Calculate()
		{
			time += Time.deltaTime * rate;

			if (time > DoublePI)
			{
				time -= DoublePI;
			}

			sin = Mathf.Sin(f: timeOffset * DoublePI + time);

			normal = 0.5f + sin * 0.5f;

			return Mathf.Lerp(a: min,
			                  b: max,
			                  t: normal);
		}


		public void Reset() => time = 0.0f;

	}

}