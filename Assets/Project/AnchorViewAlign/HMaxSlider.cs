public class HMaxSlider : DebugSlider
{

	protected override void Apply(float lerp)
	{
		base.Apply(lerp);

		ViewAlignment.i.horizontal.min = -lerp;
		ViewAlignment.i.horizontal.max = lerp;
	}

}