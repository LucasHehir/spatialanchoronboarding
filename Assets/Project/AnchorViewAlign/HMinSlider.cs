public class HMinSlider : DebugSlider
{

	protected override void Apply(float lerp)
	{
		base.Apply(lerp);

		ViewAlignment.i.horizontal.min = lerp;
	}

}