public class VMaxSlider : DebugSlider
{

	protected override void Apply(float lerp)
	{
		base.Apply(lerp);

		ViewAlignment.i.vertical.max = lerp;
	}

}