//using System;
//
//using UnityEngine;
//
//using Vanilla.DataAssets;
//
//[Serializable]
//public class EventSocket : ISocket
//{
//	
//	[SerializeField]
//	protected EventAsset _asset;
//	public EventAsset asset
//	{
//		get => _asset;
//		set => _asset = value;
//	}
//
//	public Action onBroadcast;
//
//
//	[ContextMenu("Broadcast")]
//	public void Broadcast()
//	{
//		if (AssetAssigned) 
//			_asset.Broadcast();
//		else 
//			onBroadcast?.Invoke();
//	}
//
//
//	public bool AssetAssigned => !ReferenceEquals(objA: _asset,
//	                                              objB: null);
//
//}
