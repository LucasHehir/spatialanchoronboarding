public class HSpeedSlider : DebugSlider
{

	protected override void Apply(float lerp)
	{
		base.Apply(lerp);

		ViewAlignment.i.hRateMax = lerp;
	}

}
