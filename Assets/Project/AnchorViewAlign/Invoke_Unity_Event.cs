//using System;
//
//using Cysharp.Threading.Tasks;
//
//using UnityEngine;
//using UnityEngine.Events;
//
//using Vanilla.MetaScript;
//
//[Serializable]
//public class Invoke_Unity_Event : TaskBase
//{
//
//	[SerializeField]
//	public UnityEvent @event;
//	
//	public override string GetDescription() => "Invoke a Unity Event";
//
//
//	public override UniTask Run()
//	{
//		@event.Invoke();
//
//		return default;
//	}
//
//}