#if DEBUG_VANILLA && POOLS
#define debug
#endif

using System;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;

using Object = UnityEngine.Object;

namespace Vanilla.Pools
{

	[Serializable]
	public class Pool<P, I>
		where P : Pool<P, I>
		where I : PoolItem<P, I>
	{

		public GameObject prefab;

		public Transform sceneParent;

		public int total = 20;

		[Tooltip("If true, items will be automatically activated on Get and de-activated on Retire.")]
		public bool toggleActiveOnAccess = true;

		[Tooltip("If true, items will have their root transform reset upon Retire.")]
		public bool resetTransformOnRetire = true;

		[SerializeField]
		protected internal List<I> _available = new List<I>();
		protected internal List<I> Available => _available ??= new List<I>();

		[SerializeField]
		protected internal List<I> _inUse = new List<I>();
		protected internal List<I> InUse => _inUse ??= new List<I>();


		public virtual void Fill()
		{
			if (!prefab) return;

			while (Available.Count + InUse.Count < total)
			{
				#if UNITY_EDITOR
				var newbie = ((GameObject) PrefabUtility.InstantiatePrefab(assetComponentOrGameObject: prefab,
				                                                           parent: sceneParent)).GetComponent<I>();
				#else
				var newbie = Object.Instantiate(original: prefab,
				                                position: Vector3.zero,
				                                rotation: Quaternion.identity,
				                                parent: sceneParent).GetComponent<I>();
				#endif

				newbie.gameObject.name = $"{typeof(I).Name} [{Available.Count}]";

				newbie.Pool = this as P;
				
				Available.Add(item: newbie);
			}
		}


		public virtual void Drain()
		{
			for (var i = Available.Count - 1;
			     i >= 0;
			     i--)
			{
				DestroyItem(Available[i]);
			}

			for (var i = InUse.Count - 1;
			     i >= 0;
			     i--)
			{
				DestroyItem(InUse[i]);
			}

			_available = new List<I>(total);
			_inUse     = new List<I>(total);
		}


		private void DestroyItem(I item)
		{
			if (Application.isPlaying)
			{
				UnityEngine.Object.Destroy(item.gameObject);
			}
			else
			{
				UnityEngine.Object.DestroyImmediate(item.gameObject);
			}
		}


		public I Get()
		{
			if (_available.Count < 1)
			{
				#if debug
				Debug.LogWarning($"I'm fresh out of [{typeof(I).Name}]s");
				#endif

				return null;
			}

			var item = _available[0];

			_available.Remove(item);

			_inUse.Add(item);

			#if debug
			Debug.LogWarning($"{typeof(I).Name} Get! [{_available.Count}/{total}] available.");
			#endif

			if (toggleActiveOnAccess)
			{
				item.gameObject.SetActive(true);
			}

			item.OnGet();
			
			return item;
		}


		public void Retire(I item)
		{
			if (!_inUse.Contains(item)) return;

			_inUse.Remove(item);

			_available.Add(item);

			var t = item.transform;

			t.SetParent(sceneParent);

			if (resetTransformOnRetire)
			{
				t.position    = Vector3.zero;
				t.eulerAngles = Vector3.zero;
				t.localScale  = Vector3.one;
			}

			if (toggleActiveOnAccess)
			{
				item.gameObject.SetActive(false);
			}

			item.OnRetire();
		}


		public virtual void RetireAll()
		{
			for (var i = InUse.Count - 1;
			     i >= 0;
			     i--)
			{
				Retire(item: InUse[i]);
			}
		}

	}

}