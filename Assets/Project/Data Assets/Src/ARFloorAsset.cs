//using System;
//
//using UnityEngine;
//
//using Vanilla.DataAssets;
//
//namespace Vanilla.AR
//{
//
//	[CreateAssetMenu(fileName = "New ARFloor Asset",
//	                 menuName = "Vanilla/AR/Data Assets/Reference/AR Floor")]
//	[Serializable]
//	public class ARFloorAsset : RefAsset<ARFloor, ARFloorSocket, ARFloorAsset, ARFloorProcessor>
//	{
//
//		
//
//	}
//
//	public class ARFloorSocket : RefSocket<ARFloor, ARFloorSocket, ARFloorAsset, ARFloorProcessor>
//	{
//
//		
//
//	}
//
//	public class ARFloorProcessor : RefProcessor<ARFloor, ARFloorSocket, ARFloorAsset, ARFloorProcessor>
//	{
//
//		public override ARFloor Apply(ARFloor input) => null;
//
//	}
//}