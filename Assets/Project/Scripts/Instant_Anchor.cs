﻿using System.Collections;
using System.Collections.Generic;

using Google.XR.ARCoreExtensions;

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.XR.ARFoundation;

public class Instant_Anchor : ARBehaviour
{

    public ARRaycastManager RaycastManager; // set from the Editor Inspector.

    public float _estimatedDistance = 1.0f;


    void Update()
    {
        Touch touch;

        if (Input.touchCount                         < 1 ||
            (touch = Input.GetTouch(index: 0)).phase != TouchPhase.Began)
        {
            return;
        }

        if (EventSystem.current.IsPointerOverGameObject(pointerId: touch.fingerId))
        {
            return;
        }

        ARRaycast raycast = RaycastManager.AddRaycast(screenPoint: touch.position,
                                                      estimatedDistance: _estimatedDistance);

        if (raycast != null)
        {
            // You can instantiate a 3D object here if you haven’t set Raycast Prefab in the scene.
        }
    }

}
