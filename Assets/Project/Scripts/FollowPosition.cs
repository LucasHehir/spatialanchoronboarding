using UnityEngine;

public class FollowPosition : MonoBehaviour
{

	public Transform target;

	public Vector3 offset;
	
	private Transform t;

	void Awake() => t = transform;

	void Update() => t.position = target.position + offset;

}
