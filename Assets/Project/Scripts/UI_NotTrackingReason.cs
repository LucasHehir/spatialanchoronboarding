﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class UI_NotTrackingReason : UI_Base
{

	[SerializeField]
	private NotTrackingReason _reason;
	public NotTrackingReason reason
	{
		get => _reason;
		set
		{
			if (_reason == value) return;

			_reason = value;

			UpdateText(update: $"NotTrackingReason:\t{_reason.ToString()}");
		}
	}

	void Update()
	{
		if (state == ARSessionState.SessionInitializing) reason = ARSession.notTrackingReason;
	}
	
}