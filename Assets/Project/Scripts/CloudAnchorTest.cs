﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

using Google.XR.ARCoreExtensions;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

using static UnityEngine.Debug;

public class CloudAnchorTest : MonoBehaviour
{

	public ARSessionState _sessionState;
	private ARSessionState sessionState
	{
		get => _sessionState;
		set
		{
			if (_sessionState == value) return;

			_sessionState = value;

			stateText.text = value.ToString();
		}
	}
	
	public NotTrackingReason _notTrackingReason;
	private NotTrackingReason notTrackingReason
	{
		get => _notTrackingReason;
		set
		{
			if (_notTrackingReason == value) return;

			_notTrackingReason = value;

			notTrackingText.text = value.ToString();
		}
	}

	public Text stateText;
	public Text notTrackingText;
	
	public static ARAnchor localAnchor;

	public ARCloudAnchor cloudAnchor;

	public ARAnchorManager anchorManager;

	public bool _busy;

	public GameObject anchorPrefab;

	public GameObject _anchorPrefabInstance;

	public ManualAlignment manual;

	public static void CreateLocalAnchor()
	{
		var anchorObj = new GameObject(name: "Local AR Anchor").transform;

//		#if DEBUG
//		Log(message: anchorObj.position.ToString()); // If these are already 0,0,0, you can make this
//		Log(message: anchorObj.rotation.ToString()); // all just one line => _localAnchor = new GameObject().Get
//		#endif

		anchorObj.position = Vector3.zero;
		anchorObj.rotation = Quaternion.identity;

		localAnchor = anchorObj.gameObject.AddComponent<ARAnchor>();
	}


	public static void DestroyLocalAnchor()
	{
		if (localAnchor)
		{
			#if DEBUG
			Log(message: "A Local Anchor object already exists; deleting...");
			#endif

			Destroy(obj: localAnchor.gameObject);
		}
	}


	public void CreateCloudAnchor()
	{
		cloudAnchor = anchorManager.HostCloudAnchor(anchor: localAnchor);

		ProcessCloudAnchor();
	}


	public void ResolveCloudAnchor()
	{
		var id = PlayerPrefs.GetString(key: "key",
		                               defaultValue: string.Empty);

		Log(message: $"Cloud Anchor ID Cache found [{id}]");

		cloudAnchor = anchorManager.ResolveCloudAnchorId(cloudAnchorId: id);

		ProcessCloudAnchor();
	}


	public async void ProcessCloudAnchor()
	{

		if (_busy) return;

		_busy = true;

		if (cloudAnchor == null)
		{
			_busy = false;

			#if DEBUG
			LogError(message: "Cloud anchor is DOA :( It didn't even make it to the internet.");
			#endif

			return;
		}

		while (cloudAnchor.cloudAnchorState == CloudAnchorState.TaskInProgress) await Task.Yield();

		if (cloudAnchor.cloudAnchorState != CloudAnchorState.Success)
		{
			_busy = false;

			#if DEBUG
			LogError(message: "Cloud anchor task failed.");
			#endif

			return;
		}

		#if DEBUG
		Log(message: "Cloud anchor task succeeded!");

		if (anchorPrefab == null || _anchorPrefabInstance != null) return;

		_anchorPrefabInstance = Instantiate(original: anchorPrefab,
		                                    position: Vector3.zero,
		                                    rotation: Quaternion.identity);

		_anchorPrefabInstance.transform.SetParent(parent: cloudAnchor.transform,
		                                          worldPositionStays: false);
		#endif

		PlayerPrefs.SetString(key: "key",
		                      value: cloudAnchor.cloudAnchorId);

//		manual.Align(cloudAnchor.transform.position);
		
		_busy = false;
	}


	void Update()
	{
		sessionState = ARSession.state;
		notTrackingReason = ARSession.notTrackingReason;
	}

}