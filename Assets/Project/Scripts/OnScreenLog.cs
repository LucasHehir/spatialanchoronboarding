﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.UI;

public class OnScreenLog : MonoBehaviour
{

	public static OnScreenLog i;
	
	public Text text;

	private const int cap = 20;

	private const string LogTag      = "<color=green>●</color>";
	private const string WarningTag  = "<color=yellow>●</color>";
	private const string ErrorTag    = "<color=red>●</color>";

	public List<string> logs = new List<string>(capacity: cap);

	public void OnEnable()
	{
		i = this;

		Application.logMessageReceived += (condition,
		                                   trace,
		                                   type) =>
		                                  {
			                                  switch (type)
			                                  {
				                                  case LogType.Error:
				                                  case LogType.Exception:
					                                  Error(msg: condition); break;
				                                  case LogType.Warning:   
					                                  Warning(msg: condition); break;
				                                  case LogType.Log:       
				                                  case LogType.Assert:
					                                  Log(msg: condition); break;
				                                  default: Error(msg: "Log error!"); break;
			                                  }
		                                  };
	}


	public void Log(string msg)
	{
		MakeSpace();
		
		logs.Add(item: $"\n{LogTag}{msg}");
		
		UpdateText();
	}

	public void Warning(string msg)
	{
		MakeSpace();
		
		logs.Add(item: $"\n{WarningTag} {msg}");
		
		UpdateText();
	}
	
	public void Error(string msg)
	{
		MakeSpace();
		
		logs.Add(item: $"\n{ErrorTag} {msg}");
		
		UpdateText();
	}

	private void MakeSpace()
	{
		while (logs.Count > cap-1) logs.RemoveAt(index: 0);
	}


	private void UpdateText() =>
		text.text = logs.Aggregate(seed: string.Empty,
		                           func: (current,
		                                  s1) => current + s1);


	// For testing
	#if false
	void Update()
	{
		if (string.IsNullOrEmpty(Input.inputString)) return;

		switch (Random.Range(0, 3))
		{
			case 0: Log(Input.inputString); break;
			case 1: Warning(Input.inputString); break;
			case 2: Error(Input.inputString); break;
		}
	}
	#endif
	
}
