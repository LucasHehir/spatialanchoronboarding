﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class ARSessionManager : MonoBehaviour
{

    public ARSession session;


    public void ResetARSession()
    {
        session.Reset();
    }
    
    public void ToggleARSession()
    {
        var e = session.enabled;

        e = !e;

        session.enabled = e;

        Debug.Log(message: $"ARSession enabled:\t {e}");
    }

}