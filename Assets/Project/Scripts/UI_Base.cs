﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Base : ARBehaviour
{

	[SerializeField]
	private Text _text;
	public Text Text
	{
		get => _text;
		set => _text = value;
	}

	public void UpdateText(string update) => Text.text = update;

	public void ClearText() => Text.text = string.Empty;

}