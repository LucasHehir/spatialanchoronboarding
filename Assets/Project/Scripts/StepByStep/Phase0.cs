﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class Phase0 : PhaseBase
{

	public float CheckRateTimer = 0.5f;

	public override void Awake() => FromPrevious();

	public override void FromPrevious()
	{
		base.FromPrevious();
		
		InvokeRepeating(methodName: nameof(CheckTrackingState),
		                time: CheckRateTimer,
		                repeatRate: CheckRateTimer);
	}

	public override void FromNext()
	{
		base.FromNext();
		
		InvokeRepeating(methodName: nameof(CheckTrackingState),
		                time: CheckRateTimer,
		                repeatRate: CheckRateTimer);
	}


	void CheckTrackingState()
	{
		if (ARSession.state == ARSessionState.SessionTracking)
		{
			Next();
		}
	}


	public override void Next()
	{
		CancelInvoke();
		
		base.Next();
	}

}
