﻿using System;
using System.Collections;
using System.Collections.Generic;

using Google.XR.ARCoreExtensions;

using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class Phase1 : PhaseBase
{
	
	public ARAnchorManager anchorMan;
	
	public Transform camTrans;

	[SerializeField]
	private FeatureMapQuality _quality;
	public FeatureMapQuality quality
	{
		get => _quality;
		set
		{
			if (_quality == value) return;

			_quality = value;

			QualityLevelChanged();
		}
	}

	public float checkTimerRate = 0.5f;


	public override void FromPrevious()
	{
		nextButton.interactable = false;
		
		UpdatePromptA("Find a clear and open area on the ground in front of you.");
		
		InvokeRepeating(methodName: nameof(CheckQuality),
		                time: checkTimerRate,
		                repeatRate: checkTimerRate);
		
		base.FromPrevious();
	}


	public override void FromNext()
	{
		nextButton.interactable = false;
		
		UpdatePromptA("Find a clear and open area on the ground in front of you.");
		
		InvokeRepeating(methodName: nameof(CheckQuality),
		                time: checkTimerRate,
		                repeatRate: checkTimerRate);
		
		base.FromNext();
	}





	void CheckQuality()
	{
		quality = anchorMan.EstimateFeatureMapQualityForHosting(pose: new Pose(position: camTrans.position,
		                                                                       rotation: camTrans.rotation));

		Debug.Log($"Quality is currently [{quality}]");
	}


	void QualityLevelChanged()
	{
		switch (_quality)
		{
			case FeatureMapQuality.Insufficient: UpdatePromptA("This area is insufficient.\nTry another area that has more unique details or patterns.");
				break;

			case FeatureMapQuality.Sufficient: UpdatePromptA("This area is sufficient.\nTap the Next button when you're ready to move on.");
				break;
			
			case FeatureMapQuality.Good: UpdatePromptA("This area is great!\nTap the Next button when you're ready to move on.");         
				break;
		}
		
		nextButton.interactable = _quality != FeatureMapQuality.Insufficient;
	}
	
	public override void Previous()
	{
		CancelInvoke();
		
		base.Previous();
	}


	public override void Next()
	{
		CancelInvoke();
		
		base.Next();
	}

}