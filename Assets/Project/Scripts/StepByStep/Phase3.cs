﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Phase3 : PhaseBase
{

	[Header("Radius")]
	public GameObject radiusObject;
	public  float radius       = 0.5f;
	private float radiusSqrMag = 2.5f;
	
	[Header("Pivot")]
	public Transform pivot;
	
	public float pivotScaleMin = 0.075f;
	public float pivotScaleMax = 0.025f;

	[Header("Aim Target")]
	public Transform aimTargetAnchor;
	public SkinnedMeshRenderer aimTargetMesh;
	
	public float aimTargetScaleMin = 5.0f;
	public float aimTargetScaleMax = 2.5f;

	public float aimTargetBlendExaggeration = 5.0f;
	
	[Header("Line Mesh")]
	public Transform lineMesh;
	public float lineWidthMin = 0.05f;
	public float lineWidthMax = 0.01f;
	
	[Header("AR Camera")]
	public Transform camTrans;

	[Header("Material")]
	private Material material;
    
	public Color badColor = new Color(r: 1f,
	                                  g: 0.5f,
	                                  b: 0.5f);
	public Color okColor = new Color(r: 1f,
	                                 g: 1f,
	                                 b: 0.5f);
	public Color goodColor = new Color(r: 0.5f,
	                                   g: 1f,
	                                   b: 0.5f);
    
	[Header("Output")]
	public float withinRadiusNormal;
    
	[SerializeField]
	private bool _withinRadius = false;
	public bool withinRadius
	{
		get => _withinRadius;
		set
		{
			if (_withinRadius == value) return;

			_withinRadius = value;
		    
			WithinRadiusChanged();
		}
	}
	
	private void OnValidate()
	{
		if (aimTargetAnchor)
		{
			aimTargetMesh = aimTargetAnchor.GetComponentInChildren<SkinnedMeshRenderer>(true);
		}
	}
	
	public override void Awake()
	{
		base.Awake();
	    
		material = aimTargetMesh.material;

		aimTargetMesh.material = pivot.GetComponent<MeshRenderer>().material = lineMesh.GetComponent<MeshRenderer>().material = material;
	    
		material.color = badColor;

		radiusSqrMag = radius * radius;
	}
	
	public override void FromPrevious()
	{
		base.FromPrevious();
		
		enabled = true;
	}

	public override void FromNext()
	{
		base.FromNext();

		enabled = true;
	}
	
    public override void Previous()
    {
	    enabled = false;
	    
	    base.Previous();
    }

    public override void Next()
    {
	    enabled = false;

		base.Next();
    }

    void OnEnable()
    {
	    SetAimTargetDistance(newDistance: camTrans.position.magnitude);
	    
	    radiusObject.SetActive(true);
	    aimTargetAnchor.gameObject.SetActive(true);
    }

    void OnDisable()
    {
	    radiusObject.SetActive(false);
	    aimTargetAnchor.gameObject.SetActive(false);

	    withinRadius = false;
    }

    public void SetAimTargetDistance(float newDistance) =>
	    aimTargetAnchor.localPosition = new Vector3(x: 0,
	                                          y: 0,
	                                          z: newDistance);


    void Update()
    {
	    var anchorPos = aimTargetAnchor.position;

	    var mag = anchorPos.magnitude;
	    
	    var sqrMag = anchorPos.sqrMagnitude;
	    
	    withinRadius       = sqrMag < radiusSqrMag;

	    // The reason we don't use radiusSqrMag here is because it's not linear! It has a distinct curve to it, skewing all easing towards 1.0
	    withinRadiusNormal = mag / radius;

	    // AimTarget rotation
	    
	    aimTargetAnchor.LookAt(pivot);

	    if (!withinRadius) return;

	    // LineMesh position

//	    lineMesh.position = anchorPos * 0.5f;

	    // LineMesh rotation
		    
//	    lineMesh.up = anchorPos;

	    // LineMesh scale

//	    var newLineMeshScale = Mathf.Lerp(a: lineWidthMin,
//	                                      b: lineWidthMax,
//	                                      t: withinRadiusNormal);
//
//	    lineMesh.localScale = new Vector3(x: newLineMeshScale,
//	                                      y: mag * 0.5f,
//	                                      z: newLineMeshScale);
		    
	    // Pivot scale

	    var pivScale = Mathf.Lerp(a: pivotScaleMin,
	                              b: pivotScaleMax,
	                              t: withinRadiusNormal);

	    pivot.localScale = new Vector3(x: pivScale,
	                                   y: pivScale,
	                                   z: pivScale);
		    
	    // AimTarget scale
		    
	    var newScale = Mathf.Lerp(a: aimTargetScaleMin,
	                              b: aimTargetScaleMax,
	                              t: withinRadiusNormal);

	    aimTargetAnchor.localScale = new Vector3(x: newScale,
	                                       y: newScale,
	                                       z: newScale);
		    
	    // AimTarget blend shape

	    aimTargetMesh.SetBlendShapeWeight(index: 0,
	                                      value: Mathf.Lerp(a: 100.0f,
	                                                        b: 0.0f,
	                                                        t: withinRadiusNormal * aimTargetBlendExaggeration));
		    
	    // Material colorization
		    
	    if (withinRadiusNormal < 0.5f)
	    {
		    material.color = Color.Lerp(a: goodColor,
		                                b: okColor,
		                                t: withinRadiusNormal * 2.0f);
	    }
	    else
	    {
		    var n = (withinRadiusNormal - 0.5f) * 2.0f;
			    
		    material.color = Color.Lerp(a: okColor,
		                                b: badColor,
		                                t: n);
	    }

    }

    void WithinRadiusChanged()
    {
	    if (withinRadius)
	    {
		    lineMesh.gameObject.SetActive(true);
		    pivot.gameObject.SetActive(true);
	    }
	    else
	    {
		    lineMesh.gameObject.SetActive(false);
		    pivot.gameObject.SetActive(false);
	    }
    }
	
}
