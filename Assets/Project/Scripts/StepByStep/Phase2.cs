﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class Phase2 : PhaseBase
{

	public PlaneManagerManager planeManagerManager;

	public ARRaycastManager rayManager;
	
	public ManualAlignment manualAlignment;

	public GameObject anchorPoint;
	
	public override void FromPrevious()
	{
		planeManagerManager.Toggle();

		rayManager.enabled = true;
	
		manualAlignment.Toggle();
		
		nextButton.interactable = false;

		ManualAlignment.OnSuccessfulAlignFrame += OnAlignment;

		anchorPoint.SetActive(false);

		base.FromPrevious();
	}
	
	public override void FromNext()
	{
		planeManagerManager.Toggle();
		planeManagerManager.ToggleTrackables();

		rayManager.enabled = true;

		manualAlignment.Toggle();

		nextButton.interactable = false;
		
		ManualAlignment.OnSuccessfulAlignFrame += OnAlignment;

		anchorPoint.SetActive(false);

		base.FromNext();
	}


	void OnAlignment()
	{
		anchorPoint.SetActive(true);

		nextButton.interactable = true;
	}
	
	public override void Next()
	{
		ManualAlignment.OnSuccessfulAlignFrame -= OnAlignment;

		planeManagerManager.Toggle();
		planeManagerManager.ToggleTrackables();

		rayManager.enabled = false;

		manualAlignment.Toggle();

		anchorPoint.SetActive(false);
		
		base.Next();
	}

}