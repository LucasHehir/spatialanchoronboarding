﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhaseBase : MonoBehaviour
{

	public GameObject phaseUI;

	public Text promptA;

	public Button previousButton;
	public Button nextButton;
	
	public PhaseBase previousPhase;
	public PhaseBase nextPhase;
	
	protected void UpdatePromptA(string msg) => promptA.text = msg;


	public virtual void Awake()
	{
		if (previousButton)
		{
			previousButton.onClick.AddListener(Previous);
		}

		if (nextButton)
		{
			nextButton.onClick.AddListener(Next);
		}
	}

	public virtual void Previous()
	{
		phaseUI.SetActive(false);

		if (previousPhase)
		{
			previousPhase.FromNext();
		}
	}

	public virtual void FromPrevious()
	{
		phaseUI.SetActive(true);
	}


	public virtual void Next()
	{
		phaseUI.SetActive(false);

		if (nextPhase)
		{
			nextPhase.FromPrevious();
		}
	}
	
	public virtual void FromNext()
	{
		phaseUI.SetActive(true);
	}
}
