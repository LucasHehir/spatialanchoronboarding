﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

public class UI_ARSessionState : UI_Base
{

	protected override void SessionStateChanged(ARSessionStateChangedEventArgs args)
	{
		base.SessionStateChanged(args: args);

		UpdateText(update: $"ARSessionState:\t{args.state.ToString()}");

	}
	
}