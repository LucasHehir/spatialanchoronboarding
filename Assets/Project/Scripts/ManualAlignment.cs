﻿using System;
using System.Collections;
using System.Collections.Generic;

using Cysharp.Threading.Tasks;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

using static UnityEngine.Debug;


public class ManualAlignment : MonoBehaviour
{

	public ARRaycastManager raycastManager;

	public List<ARRaycastHit> results = new List<ARRaycastHit>();

	public static Action OnSuccessfulAlignFrame;

	[SerializeField]
	private Camera _cam;
	public Camera cam => _cam ? _cam : _cam = Camera.main;
	
	[SerializeField]
	private Transform _camTrans;
	public Transform camTrans =>
		_camTrans ?
			_camTrans :
			_camTrans = cam.transform;

	[SerializeField]
	private Transform _offset;
	public Transform offset
	{
		get
		{
			if (!ReferenceEquals(objA: _offset,
			                     objB: null)) return _offset;

			_offset = new GameObject(name: "Session Offset").transform;

			_offset.SetParent(parent: transform,
			                  worldPositionStays: false);

			for (var i = 0;
			     i < transform.childCount;
			     ++i)
			{
				var child = transform.GetChild(index: i);

				if (child == _offset) continue;

				child.SetParent(parent: _offset,
				                worldPositionStays: true);

				--i;
			}

			return _offset;
		}
	}

	[SerializeField, Range(min: 1.0f,
	                       max: 10.0f)]
	private float _maxDistance = 5.0f;
	public float maxDistance => _maxDistance;


	public bool makeContentFaceCamera = true;

	public void Update()
	{
		#if UNITY_EDITOR
			if (!Input.GetKey(KeyCode.Mouse0)) return;

			var pretendFloorUnderUser = camTrans.position + Vector3.down;

			var editorPlane = new Plane(a: pretendFloorUnderUser,
			                            b: pretendFloorUnderUser + Vector3.forward,
			                            c: pretendFloorUnderUser + Vector3.right);

			var ray = cam.ScreenPointToRay(Input.mousePosition);

			if (!editorPlane.Raycast(ray: ray,
			                         enter: out var debugEditorHitDistance) ||
			    debugEditorHitDistance > maxDistance)
				return;

			var alignPoint = ray.GetPoint(distance: debugEditorHitDistance);
		#else
			if (Input.touchCount == 0) return;

			var touch = Input.GetTouch(index: 0);

			if (EventSystem.current.IsPointerOverGameObject(pointerId: touch.fingerId)) return;

			if (!raycastManager.Raycast(screenPoint: touch.position,
			                            hitResults: results,
			                            trackableTypes: TrackableType.PlaneWithinInfinity)) return;

			var alignPoint = new Vector3(x: 0,
			                             y: 100.0f,
			                             z: 0);
			
			foreach (var r in results)
			{
				if (r.distance        < maxDistance &&
				    r.pose.position.y < alignPoint.y)
				{
					alignPoint = r.pose.position;
				}
			}
			
//			if (results[index: 0].distance > maxDistance)
//			{
//				return;
//			}

//			var alignPoint = results[index: 0].pose.position;
		#endif

		Align(hit: alignPoint);
	}


	public void Align(Vector3 hit)
	{
		offset.position -= hit;

		OnSuccessfulAlignFrame?.Invoke();

		if (!makeContentFaceCamera) return;

		var camDir = offset.localPosition + camTrans.localPosition;

		camDir.y = 0.0f;

		transform.rotation = Quaternion.Inverse(rotation: Quaternion.LookRotation(forward: camDir));
	}

//	void OnEnable() => CloudAnchorTest.DestroyLocalAnchor();

//	void OnDisable() => CloudAnchorTest.CreateLocalAnchor();

	public void Toggle()
	{
		var e = enabled;

		e = !e;

		enabled = e;
		
		Log(message: $"Manual Alignment: [{e}]");
	}

}