﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

using Google.XR.ARCoreExtensions;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

public class ARBehaviour : MonoBehaviour
{

	public static ARSessionState state;

	public static Action<ARSessionState, ARSessionState> OnStateChange;
	
	public void OnEnable() => ARSession.stateChanged += SessionStateChanged;

	public void OnDisable() => ARSession.stateChanged -= SessionStateChanged;

	protected virtual void SessionStateChanged(ARSessionStateChangedEventArgs args)
	{
		OnStateChange?.Invoke(arg1: state,
		                      arg2: args.state);
		
		state = args.state;
	}

}
