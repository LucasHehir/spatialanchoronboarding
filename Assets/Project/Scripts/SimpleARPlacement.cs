using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class SimpleARPlacement : MonoBehaviour
{

	public ARFloor floor;

	public Transform anchorBlueprint;

	public Transform camTrans;
	
	public ARRaycastManager rayMan;

	private bool _firstPlacement;
	public bool firstPlacement
	{
		get => _firstPlacement;
		set
		{
			if (_firstPlacement == value) return;

			_firstPlacement = value;
			
			floor.GetComponent<MeshRenderer>().enabled = true;
		}
	}
	
	public List<ARRaycastHit> results = new List<ARRaycastHit>();

	public bool updating;
	
	async void Start()
	{
		floor.GetComponent<MeshRenderer>().enabled = false;
		
		while (ARSession.state != ARSessionState.SessionTracking)
		{
			await Task.Yield();
		}

		updating = true;
	}
	
	public void Update()
	{
		if (!updating) return;
		
		if (Input.touchCount == 0) return;

		var touch = Input.GetTouch(index: 0);

		if (EventSystem.current.IsPointerOverGameObject(pointerId: touch.fingerId)) return;

		if (!rayMan.Raycast(screenPoint: touch.position,
		                            hitResults: results,
		                            trackableTypes: TrackableType.PlaneEstimated)) return;

		Align(hit: results[index: 0]);
	}


	public void Align(ARRaycastHit hit)
	{
		if (hit.distance > 10.0f) return;

		firstPlacement = true;
		
		var p = hit.pose.position;

		floor.transform.localPosition = new Vector3(x: 0,
		                                            y: p.y,
		                                            z: 0);

		anchorBlueprint.position = p;

		var dirToCam = camTrans.position - p;

		dirToCam.y = 0.0f;
		
//		dirToCam.Normalize();

		anchorBlueprint.forward = dirToCam;
	}


	public void Toggle() => enabled = !enabled;

}
