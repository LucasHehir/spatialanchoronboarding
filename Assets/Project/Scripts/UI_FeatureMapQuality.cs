﻿using System.Collections;
using System.Collections.Generic;

using Google.XR.ARCoreExtensions;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

public class UI_FeatureMapQuality : UI_Base
{
	
	public ARAnchorManager anchorMan;

	public Transform camTrans;

	[SerializeField]
	private FeatureMapQuality _quality;
	public FeatureMapQuality quality
	{
		get => _quality;
		set
		{
			if (_quality == value) return;

			_quality = value;

			UpdateText(update: $"FeatureMapQuality:\t{quality.ToString()}");
		}
	}


	void Update()
	{
		if (state == ARSessionState.SessionTracking)
			quality = anchorMan.EstimateFeatureMapQualityForHosting(pose: new Pose(position: camTrans.position,
			                                                                       rotation: camTrans.rotation));
	}
	
}