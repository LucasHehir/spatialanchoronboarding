﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Vanilla.Geodetics;

public class OrbitGrid : MonoBehaviour
{

//	public Transform target;

	public Vector3 current;

	public GameObject panelPrefab;

	public int columns = 8;
	public int rows    = 2;

//	public float rowPadding = 0.2617967f;
	
	public GameObject[] panels;

	public float polarTotalArea   = 6.283186f;
	public float azimuthTotalArea = 0.785398f;

	public float shrinkRowsBy = 0.9f;
	
	public void Start()
	{
		panels = new GameObject[columns * rows - 1];

		var polarSplit   = polarTotalArea   / columns;
		var azimuthSplit = azimuthTotalArea / (rows + 1);

		var scale = panelPrefab.transform.localScale;

		var i = 0;
		
		for (var y = 0;
		     y < rows;
		     y++)
		{
			current.x += azimuthSplit * 0.75f;

			current.y = y % 2 == 0 ? 0.0f : polarSplit * 0.5f;
//			current.polar = 0.0f;
			
			for (var x = 0;
			     x < (y % 2 == 0 ? columns : columns-1);
			     x++)
			{
				var newPanel = Instantiate(original: panelPrefab,
				                           position: transform.position + current.GeodeticToCartesian(),
				                           rotation: Quaternion.identity,
				                           parent: transform);

				newPanel.transform.localScale = scale;
				
				newPanel.name = $"Panel [{y * columns + x}] [{x},{y}]";
				
				panels[i] = newPanel;
				
				newPanel.transform.LookAt(worldPosition: transform.position);

				current.y += polarSplit;

				i++;
			}

			current.x += azimuthSplit;

			current.y += polarSplit * 0.75f;

			scale *= shrinkRowsBy;
			
//			scale = new Vector3(x: scale.x - shrinkRowsBy,
//			                    y: scale.y - shrinkRowsBy,
//			                    z: scale.z - shrinkRowsBy);
		}
	}


	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.blue;

		Gizmos.DrawSphere(center: transform.position + current.GeodeticToCartesian(),
		                  radius: 0.25f);
	}


	void Update()
	{
//		current.FromCartesian(transform.position);
	}

}
