﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//using Vanilla.Math;

public class UIDistanceScaler : MonoBehaviour
{

    public AnimationCurve distanceCurve;

    public Transform camTrans;
    public Transform target;

    public float distMinInMetres = 2.0f;
    public float distMaxInMetres = 5.0f;

//    public float sqrMagDist;
    public float distSqrMag;

    public float sqrMagMin = 4.0f;
    public float sqrMagMax = 25.0f;
    
    // The scaling ratio is inverted
    public float UIScaleMin = 2.5f;
    public float UIScaleMax = 1.0f;

    [Range(min: 0,
           max: 1)]
    public float distSqrMagNormal = 0.5f;

    public float easedDistSqrMagNormal;
    
    public float UIScale;

//    public EasingMethodSlot easing;
    
    public RectTransform targetImage;

    public float algorithmCutoff = 0.5f;

//    void Awake() => easing.FetchMethod();


//    private void OnValidate()
//    {
//        if (Application.isPlaying)
//        {
//            easing.FetchMethod();
//        }
//    }


    void Update()
    {
        distSqrMag = (target.position - camTrans.position).sqrMagnitude;

        sqrMagMin = distMinInMetres * distMinInMetres;
        sqrMagMax = distMaxInMetres * distMaxInMetres;
        
        distSqrMagNormal = (distSqrMag - sqrMagMin) / (sqrMagMax - sqrMagMin);

        // Cheat using an animation curve... unnecessary if we just find the right easing algorithm
        
        easedDistSqrMagNormal = distanceCurve.Evaluate(distSqrMagNormal);

		// Sine ease out - not quite...

//		const float piSlice = Mathf.PI * 0.5f;
//		
//		easedDistSqrMagNormal = Mathf.Sin(distSqrMagNormal * piSlice);

        // Quadratic ease out -- closest
        
//        easedDistSqrMagNormal = -(distSqrMagNormal * (distSqrMagNormal - 2)); // This is pretty close & very cheap!

        // Cubic ease out - very close
        
//        var f = distSqrMagNormal - 1;
        
//        easedDistSqrMagNormal = f * f * f + 1;

        // Combined - One half of Cubic and one half of Quadratic
        
//        var f = distSqrMagNormal - 1;

//        easedDistSqrMagNormal = distSqrMagNormal > algorithmCutoff ? -(distSqrMagNormal * (distSqrMagNormal - 2)) : easedDistSqrMagNormal = f * f * f + 1;
        
        // Quartic ease out - too sharp
//        
//        var f = distSqrMagNormal - 1;
//        
//        easedDistSqrMagNormal =  f * f * f * (1 - distSqrMagNormal) + 1;
        
        // Re-map to UI Scale

        UIScale = Mathf.Lerp(a: UIScaleMin,
                             b: UIScaleMax,
                             t: easedDistSqrMagNormal);
        
        targetImage.localScale = new Vector3(x: UIScale,
                                             y: UIScale,
                                             z: UIScale);

    }
    
//    // It's Carmack time babyyyyyyyyyy
//    public unsafe float Q_rsqrt(float number)
//    {
//        long i;
//
//        float x2,
//              y;
//
//        const float threehalfs = 1.5F;
//
//        x2 = number * 0.5F;
//        y  = number;
//        i  = *(long*) &y;           // evil floating point bit level hacking
//        i  = 0x5f3759df - (i >> 1); // what the fuck? 
//        y  = *(float*) &i;
//        y  = y * (threehalfs - (x2 * y * y)); // 1st iteration
//
//        return y;
//    }

}
