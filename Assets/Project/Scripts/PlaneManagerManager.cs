﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class PlaneManagerManager : MonoBehaviour
{

	public ARPlaneManager manager;

	public bool trackablesActive = true;


	public void Toggle()
	{
		var e = manager.enabled;

		e = !e;

		manager.enabled = e;

		Debug.Log(message: $"PlaneManager enabled: [{e}]");
	}


	public void ToggleTrackables()
	{
		trackablesActive = !trackablesActive;
		
		manager.SetTrackablesActive(active: trackablesActive);
		
		Debug.Log(message: $"Planes active: [{trackablesActive}]");
	}

}