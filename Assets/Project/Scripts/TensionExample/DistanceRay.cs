﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class DistanceRay : MonoBehaviour
{
	
	[Header("Radius")]
	public GameObject radiusObject;
	public  float radius       = 0.5f;
	private float radiusSqrMag = 2.5f;
	
	[Header("Pivot")]
	public Transform pivot;
	
	public float pivotScaleMin = 0.075f;
	public float pivotScaleMax = 0.025f;

	[Header("Aim Target")]
	public Transform aimTarget;
	public SkinnedMeshRenderer aimTargetSkin;
	
	public float               aimTargetScaleMin = 5.0f;
	public float               aimTargetScaleMax = 2.5f;

	public float aimTargetBlendExaggeration = 5.0f;
	
	[Header("Line Mesh")]
	public Transform lineMesh;
	public float lineWidthMin = 0.05f;
	public float lineWidthMax = 0.01f;
	
	[Header("AR Camera")]
    public Transform camTrans;

    [Header("Material")]
    private Material material;
    
    public Color badColor  = new Color(r: 1f,
                                       g: 0.5f,
                                       b: 0.5f);
    public Color okColor   = new Color(r: 1f,
                                       g: 1f,
                                       b: 0.5f);
    public Color goodColor = new Color(r: 0.5f,
                                       g: 1f,
                                       b: 0.5f);

    [Header("Debug")]
    public Text debugText1;
    public Text debugText2;
    public Text debugText3;
    
    [Header("Output")]
    public float withinRadiusNormal;
    
    [SerializeField]
    private bool _withinRadius = false;
    public bool withinRadius
    {
	    get => _withinRadius;
	    set
	    {
		    if (_withinRadius == value) return;

		    _withinRadius = value;
		    
		    WithinRadiusChanged();
	    }
    }
    
    private void OnValidate()
    {
	    if (aimTarget)
	    {
		    aimTargetSkin = aimTarget.GetComponentInChildren<SkinnedMeshRenderer>(true);
	    }
    }


    public void Toggle() => enabled = !enabled;


    void Awake()
    {
	    material = aimTargetSkin.material;

	    aimTargetSkin.material = pivot.GetComponent<MeshRenderer>().material = lineMesh.GetComponent<MeshRenderer>().material = material;
	    
	    material.color = badColor;

	    radiusSqrMag = radius * radius;
    }

    public void OnEnable()
    {
	    SetAimTargetDistance(newDistance: camTrans.position.magnitude);
	    
	    radiusObject.SetActive(true);
	    aimTarget.gameObject.SetActive(true);
    }


    private void OnDisable()
    {
	    radiusObject.SetActive(false);
		aimTarget.gameObject.SetActive(false);

		withinRadius = false;
    }


    public void SetAimTargetDistance(float newDistance) =>
	    aimTarget.localPosition = new Vector3(x: 0,
	                                          y: 0,
	                                          z: newDistance);


    void LateUpdate()
    {
	    var anchorPos = aimTarget.position;

	    var mag = anchorPos.magnitude;
	    
	    var sqrMag = anchorPos.sqrMagnitude;
	    
	    withinRadius       = sqrMag < radiusSqrMag;

	    // The reason we don't use radiusSqrMag here is because it's not linear! It has a distinct curve to it, skewing all easing towards 1.0
	    withinRadiusNormal = mag / radius;

	    // AimTarget rotation
	    
	    aimTarget.LookAt(pivot);

	    debugText1.text = $"Within Radius: [{withinRadius}]";
	    debugText2.text = $"Within Radius Normal: [{withinRadiusNormal}]";

	    if (!withinRadius) return;

	    // LineMesh position

	    lineMesh.position = anchorPos * 0.5f;

	    // LineMesh rotation
		    
	    lineMesh.up = anchorPos;

	    // LineMesh scale

	    var newLineMeshScale = Mathf.Lerp(a: lineWidthMin,
	                                      b: lineWidthMax,
	                                      t: withinRadiusNormal);

	    lineMesh.localScale = new Vector3(x: newLineMeshScale,
	                                      y: mag * 0.5f,
	                                      z: newLineMeshScale);
		    
	    // Pivot scale

	    var pivScale = Mathf.Lerp(a: pivotScaleMin,
	                              b: pivotScaleMax,
	                              t: withinRadiusNormal);

	    pivot.localScale = new Vector3(x: pivScale,
	                                   y: pivScale,
	                                   z: pivScale);
		    
	    // AimTarget scale
		    
	    var newScale = Mathf.Lerp(a: aimTargetScaleMin,
	                              b: aimTargetScaleMax,
	                              t: withinRadiusNormal);

	    aimTarget.localScale = new Vector3(x: newScale,
	                                       y: newScale,
	                                       z: newScale);
		    
	    // AimTarget blend shape

	    aimTargetSkin.SetBlendShapeWeight(index: 0,
	                                      value: Mathf.Lerp(a: 100.0f,
	                                                        b: 0.0f,
	                                                        t: withinRadiusNormal * aimTargetBlendExaggeration));
		    
	    // Material colorization
		    
	    if (withinRadiusNormal < 0.5f)
	    {
		    material.color = Color.Lerp(a: goodColor,
		                                b: okColor,
		                                t: withinRadiusNormal * 2.0f);
	    }
	    else
	    {
		    var n = (withinRadiusNormal - 0.5f) * 2.0f;
			    
		    material.color = Color.Lerp(a: okColor,
		                                b: badColor,
		                                t: n);
	    }

    }

    void WithinRadiusChanged()
    {
	    if (withinRadius)
	    {
		    lineMesh.gameObject.SetActive(true);
		    pivot.gameObject.SetActive(true);
	    }
	    else
	    {
		    lineMesh.gameObject.SetActive(false);
		    pivot.gameObject.SetActive(false);
	    }
    }
}
