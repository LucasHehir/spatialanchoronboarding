using System;

using UnityEngine;

public class Earth_Sunlight : MonoBehaviour
{

    public GameObject sun;

    private const float SECONDSOFADAY = 86400;
    private const float SECOND        = 1;
    private const float MINUTE        = 60 * SECOND;
    private const float HOUR          = 60 * MINUTE;
    private const float DAY           = 24 * HOUR;

    private const float DEGREES_PER_SECOND = 360 / SECONDSOFADAY;

    private float _SessionTime;


    // Use this for initialization
    void Start()
    {
        Debug.Log("Hour: "                                       + DateTime.Now.Hour        + " Minute: " + DateTime.Now.Minute + " Second: " + DateTime.Now.Second);
        
        var alreadyPassedSeconds = DateTime.Now.Hour * 60 * 60 + DateTime.Now.Minute * 60 + DateTime.Now.Second;
        var startRotation        = DEGREES_PER_SECOND * alreadyPassedSeconds;
        
        Debug.Log("StartRotation: " + startRotation);

        sun.transform.rotation = Quaternion.Euler(0,
                                                  startRotation,
                                                  0);

        _SessionTime = 0;
    }


    // Update is called once per frame
    void Update()
    {
        sun.transform.Rotate(new Vector3(0,
                                         DEGREES_PER_SECOND,
                                         0) *
                             Time.deltaTime);

        _SessionTime += Time.deltaTime;
    }

}