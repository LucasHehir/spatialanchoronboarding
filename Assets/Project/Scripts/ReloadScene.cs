﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReloadScene : MonoBehaviour
{

	public void ReloadSceneNow() =>
		SceneManager.LoadScene(sceneBuildIndex: 0,
		                       mode: LoadSceneMode.Single);

}
