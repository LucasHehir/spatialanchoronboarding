﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnchorBlueprintSession : MonoBehaviour
{

	public GameObject anchor;
	public GameObject canvas;
	public GameObject distanceometer;
	
	public void Toggle() => enabled = !enabled;


	private void OnEnable()
	{
		anchor.SetActive(true);
		canvas.SetActive(true);
		distanceometer.SetActive(true);
	}


	private void OnDisable()
	{
		anchor.SetActive(false);
		canvas.SetActive(false);
		distanceometer.SetActive(false);
	}

}
