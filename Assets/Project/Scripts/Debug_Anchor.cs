﻿using System.Collections;
using System.Collections.Generic;

using Google.XR.ARCoreExtensions;

using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class Debug_Anchor : MonoBehaviour
{

	[SerializeField]
	private ARAnchor _anchor;
	public  ARAnchor anchor => _anchor ? _anchor : _anchor = GetComponent<ARAnchor>();


	public void Start()
	{
		
	}

}
