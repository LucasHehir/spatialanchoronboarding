﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DistanceOMeter : MonoBehaviour
{

	[Header("References")]
    public Transform camTrans;

    public Transform target;

    public Transform canvas;
    
    public Text text;

    public LineRenderer line;

    public RectTransform p1Arrow;
    public RectTransform p2Arrow;
    
    [Header("Settings")]
    public float heightOffTheGround = 0.1f;

    public float radius = 0.5f;

    [Tooltip("This should be half the physical height of the sprite (world-space UI is measured in metres!)")]
    public float spriteOffset = 0.125f;
    
    [Tooltip("How far, in metres, should each sprite sit from each end?")]
    public float lineMargin = 0.1f;
    
    public float minimumDistanceReading = 0.5f;

    public float maximumDistanceReading = 4.0f;

    public float lineMinimum = 0.5f;

    [Header("Output")]
    public float userDistance;
    
    public bool tooClose;

    public bool tooFar;

    void LateUpdate()
    {
	    var userPos   = camTrans.position;
	    
	    userDistance = userPos.sqrMagnitude;
	    
	    tooClose = userDistance < minimumDistanceReading * minimumDistanceReading;

	    tooFar = userDistance > maximumDistanceReading * maximumDistanceReading;

	    text.text = $"{userPos.magnitude:F1}m"; // Get this first so other calculations aren't factored in

	    var userPoint = new Vector3(x: userPos.x,
	                                y: 0.0f,
	                                z: userPos.z);
	    
	    var userDir = userPoint.normalized;

	    var edgePoint = userDir * radius;

	    var margin = userDir * (spriteOffset + lineMargin);

	    var p1 = edgePoint + margin;

	    var p2 = userPoint - margin;

	    if (p2.sqrMagnitude < lineMinimum * lineMinimum)
	    {
		    p2 = userDir * lineMinimum;
	    }
	    
	    var midPoint = Vector3.Lerp(a: p1,
	                                b: p2,
	                                t: 0.5f);
	    
	    midPoint.y = heightOffTheGround;
	    p1.y = heightOffTheGround;
	    p2.y = heightOffTheGround;
	    
	    // Move transforms into their positions

	    target.position = midPoint;
	    
	    canvas.LookAt(Vector3.zero);

	    var e = canvas.eulerAngles;

	    e.x = 0.0f;
	    e.z = 0.0f;

	    canvas.eulerAngles = e;

//	    target.eulerAngles = new Vector3(x: 0,
//	                                     y: camTrans.eulerAngles.y,
//	                                     z: 0);
	    
	    line.SetPosition(index: 0,
	                     position: p1);

	    line.SetPosition(index: 1,
	                     position: p2);

	    p1Arrow.position = p1;
	    p2Arrow.position = p2;
    }

}
